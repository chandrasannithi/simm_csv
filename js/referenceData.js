angular.module('myApp.refData',[])
     .controller('reference_data_viewController',['$scope','$rootScope','refData',
                    function($scope,$rootScope,refData){
                    $scope.title = "Reference Data Views : Calenders";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    $scope.curveTitles = refData.curvTitle();
                    $scope.curveTableData = refData.curvData();
                        $scope.statusLength = $scope.curveTableData.length;
                    $scope.calenderCountry = refData.country();
                    $scope.holidaysHeading = refData.holidayHeading();
                    $scope.holidayLists = refData.holidayList();
                    $scope.priceHeaders = refData.priceHeader();
                    $scope.priceContents = refData.priceContent();
                        $scope.priceLength = $scope.priceContents.length;
                    $scope.generalTableHeaders = refData.generalTableHeader();
                    $scope.generalTableContents = refData.generalTableContent();
                        $scope.generalLength = $scope.generalTableContents.length;
                    
                        
                    //GIRR
                    $scope.girrTbl1 = refData.girrTbl1();
                        $scope.girrlLength = $scope.girrTbl1.length;
                    
                    $scope.girrTbl2 = refData.girrTbl2();
                         $scope.girr2lLength = $scope.girrTbl2.length;
                        
                    $scope.girrTbl3 = refData.girrTbl3();
                         $scope.girr3lLength = $scope.girrTbl3.length;
                        
                    $scope.girrTbl4 = refData.girrTbl4();
                         $scope.girr4lLength = $scope.girrTbl4.length;
                        
                    
                    //Credit Qualifying
                        
                    $scope.creditTbl1 = refData.credTbl1();
                         $scope.creditTbl1Length = $scope.creditTbl1.length;
                    
                    $scope.creditTbl2 = refData.credTbl2();
                        $scope.creditTbl2Length = $scope.creditTbl2.length;
                        
                    $scope.creditTbl3 = refData.credTbl3();
                        $scope.creditTbl3Length = $scope.creditTbl3.length;
                        
                    $scope.creditTbl4 = refData.credTbl4();
                         $scope.creditTbl4Length = $scope.creditTbl4.length;
                        
                    // Credit Non Quailfying
                        
                    $scope.nonCreditTbl1 = refData.nonCreditTbl1();
                        $scope.nonCreditTbl1Length = $scope.nonCreditTbl1.length;
                    
                    $scope.nonCreditTbl2 = refData.nonCreditTbl2();
                        $scope.nonCreditTbl2Length = $scope.nonCreditTbl2.length;
                        
                    $scope.nonCreditTbl3 = refData.nonCreditTbl3();
                         $scope.nonCreditTbl3Length = $scope.nonCreditTbl3.length;
                        
                    $scope.nonCreditTbl4 = refData.nonCreditTbl4();
                        $scope.nonCreditTbl4Length = $scope.nonCreditTbl4.length;
                        
                    //Commodity
                    $scope.commodityTbl1 = refData.comodity1();
                         $scope.commodityTbl1Length = $scope.commodityTbl1.length;
                    $scope.commodityTbl2 = refData.comodity2();
                        $scope.commodityTbl2Length = $scope.commodityTbl2.length;
                    $scope.commodityTbl3 = refData.comodity3();   
                        $scope.commodityTbl3Length = $scope.commodityTbl3.length;
                    
                    $scope.commodityTbl4 = refData.comodity4();
                        $scope.commodityTbl4Length = $scope.commodityTbl4.length;
                    $scope.commodityTbl5 = refData.comodity5();
                         $scope.commodityTbl5Length = $scope.commodityTbl5.length;
                        
                    //FX
                    $scope.fxs = refData.fxs();
                        $scope.fxsLength = $scope.fxs.length;
                        
                    
                }])