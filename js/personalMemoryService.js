angular.module('myApp.personalMS',[])

//Personal Control Starts
    
            .factory('personControl',function(){
        
                return{
                    query: function()
                    {
                        return portfoliosData;
                    },
                    branchRul: function()
                    {
                        return branchRules;
                    }
                }
            })
            //Personal Control Ends


  var portfoliosData = [
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1234 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1235 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1236 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1237 },
        {"CounterParty":"Bank of America", "Portfolio":5555 },
        {"CounterParty":"Bank of America", "Portfolio":5557 },
        {"CounterParty":"Bank of America", "Portfolio":5558 },
        {"CounterParty":"Bank of America", "Portfolio":5559 }
        
    ];

var branchRules = [
        {"CounterParty":"JP Morgan & Chase","Portfolio":1234,"SIMMThreshold":1000000},
        {"CounterParty":"Citigroup","Portfolio":1235,"SIMMThreshold":500000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1236,"SIMMThreshold":750000},
        {"CounterParty":"Barclays PLC","Portfolio":1237,"SIMMThreshold":125000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1238,"SIMMThreshold":750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1239,"SIMMThreshold":1750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1243,"SIMMThreshold":5750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1241,"SIMMThreshold":5750000}
    ];