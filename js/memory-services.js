'use strict';

(function(){
    
    //Dashboard table content
/*	var companyDetails = [
			{"CompanyParty":"JP Morgan", "SIMMRates":1323674.72 , "SIMMCredit": 512909.02, "SIMMEquity": 2774794.94, "SIMMCommodity":0, "TotalSIMM":4611378.68},
			{"CompanyParty":"Citigroup", "SIMMRates": 0, "SIMMCredit": 0, "SIMMEquity": 0, "SIMMCommodity":512909.02, "TotalSIMM":512909.02},
			{"CompanyParty":"Total", "SIMMRates": "", "SIMMCredit": "", "SIMMEquity": "", "SIMMCommodity":"", "TotalSIMM":5124287.70}			
			
			];   
   */
   
   
    
    
    //Dashboard table Heading
    var dashboardHeaders = [{"party":"Counter Party", "rates":"SIMM-Rates and FX", "credit":"SIMM-Credit","equity":"SIMM-Equity","commodity":"SIMM-Commodity","total":"Total SIMM","cpty":"CPTY Details"}];
    
    
    // Counter2 table content
    var productsList = [
				{"RiskCategory":"Commodity", "CounterParty":"Citigroup" , "CSAID":"CITI01" , "TradingDesk":"US Commodity", "Currency":"USD", "DeltaMargin":11251118.55 ,    		                 "VegaMargin":1248366.73,"CurvatureMargin":1073751.64,"Total":323253.64,"Trades":""},
				
				{"RiskCategory":"Rates And FX", "CounterParty":"JP Morgan" ,"CSAID":"JPM01" ,"TradingDesk":"FX Forward" ,"Currency":"EUR" ,"DeltaMargin":500253.11 ,  "VegaMargin":500167.97,"CurvatureMargin":323253.64,"Total":323253.64,"Trades": ""},
				
				{"RiskCategory":"Equity", "CounterParty":"JP Morgan" ,"CSAID":"JPM02" ,"TradingDesk":"Prop trading" ,"Currency":"USD" ,"DeltaMargin":984007.93 ,"VegaMargin":982784.74,"CurvatureMargin":808002.27,"Total":323253.64,"Trades": ""},
				
				{"RiskCategory":"Credit", "CounterParty":"JP Morgan" ,"CSAID":"JPM02" ,"TradingDesk":"Prop trading" ,"Currency":"EUR" ,"DeltaMargin":685862.81 ,"VegaMargin":-685459.48,"CurvatureMargin":512505.69,"Total":323253.64,"Trades": ""},
				
				{"RiskCategory":"Total:", "CounterParty":"" ,"CSAID":"" ,"TradingDesk":"" ,"Currency":'0.00' ,"DeltaMargin":13421242.40 ,"VegaMargin":2045859.96 ,"CurvatureMargin":2717513.24,"Total":"" ,"Trades": ""}
			
			];
    
    // Counter2 table heading
    var counter2Headers = [
        {"risk":"Risk Category", "counter": "Counter Party", "csaID":"CSA ID","trading":"Trading Desk","currency":"Currency","delta":"Delta Margin","vega":"Vega Margin","curvature":"Curvature Margin","total":"Total","trades":"Trades"}
    ];
    
    
    // Counter1 Table Content    
    var Counter1ProductList = [
					{"RiskCategory":"Commodity" ,"CounterParty":"City Group1" ,"Portfolio":4567 ,"Currency":"USD" , "DeltaMargin":1125118.55 , "VegaMargin":1248366.73 , "CurvatureMargin":1073751.64 ,"Total":3447236.92 ,"Trades": ""},
					
					{"RiskCategory":"Total:" ,"CounterParty":"" ,"Portfolio":"" ,"Currency":"" , "DeltaMargin":1125118.55 , "VegaMargin":1248366.73 ,"CurvatureMargin":1073751.64 , "Total":3447236.92 ,"Trades": ""}
				
				
				];
    
    // Commodity Table Content
    var commodities = [
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID": 12345 ,"ProductType": "Commodity" ,"SubProduct": "Forward" ,"RiskFactorType": "CMD" ,"DeltaMargin":31253.29,"VegaMargin":34676.85,"CurvatureMargin":29826.43,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12347 ,"ProductType": "Commodity" ,"SubProduct": "Forward" ,"RiskFactorType": "CMD" ,"DeltaMargin":62506.59,"VegaMargin":69353.71,"CurvatureMargin":59652.87,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12349 ,"ProductType": "Commodity" ,"SubProduct": "Forward" ,"RiskFactorType": "CMD" ,"DeltaMargin": 93759.87,"VegaMargin":104030.56,"CurvatureMargin":89479.30,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12351 ,"ProductType": "Commodity" ,"SubProduct": "Forward" ,"RiskFactorType": "CMD" ,"DeltaMargin": 125013.17,"VegaMargin":138707.41,"CurvatureMargin":119305.74,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12353 ,"ProductType": "Commodity" ,"SubProduct": "Swap" ,"RiskFactorType": "CMD" , "DeltaMargin":156266.47,"VegaMargin":173384.27,"CurvatureMargin":149132.17,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12355 ,"ProductType": "Commodity" ,"SubProduct": "Swap" ,"RiskFactorType": "CMD" ,"DeltaMargin":187519.76,"VegaMargin":208061.12,"CurvatureMargin":178958.61,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12357 ,"ProductType": "Commodity" ,"SubProduct": "Swap" ,"RiskFactorType": "CMD" ,"DeltaMargin":218773.05,"VegaMargin":242737.98,"CurvatureMargin":208785.04,"TradeDate":'01-05-16' },
				
				{"CounterParty": "City Group1" ,"Poortfolio": 4567 ,"DealID":12359 ,"ProductType": "Commodity" ,"SubProduct": "Swap" ,"RiskFactorType": "CMD" ,"DeltaMargin":250026.34,"VegaMargin":277414.83,"CurvatureMargin":238611.48,"TradeDate":'01-05-16' },
				
				{"CounterParty": "Total:" ,"Poortfolio": "" ,"DealID": "" ,"ProductType": "" ,"SubProduct": "" ,"RiskFactorType": "" ,"DeltaMargin":1125118.54,"VegaMargin":1248366.73,"CurvatureMargin":1073751.64,"TradeDate":"" }
				
				
				];
    
    // Credit Trade table content
    
    var creditTrades = [
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"joe", "DealID":23451,"ProductType":"Credit"  ,"SubProduct": "CDS"  ,"RiskFactorType": "CSNQ"  ,"DeltaMargin":22862.09 ,"VegaMargin":-22848.65,"CurvatureMargin":17083.523,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"joe", "DealID":23456,"ProductType": "Credit" ,"SubProduct": "CDS" ,"RiskFactorType": "CSNQ" ,"DeltaMargin":45724.19 ,"VegaMargin":-45697.30,"CurvatureMargin":34167.05,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"Mary", "DealID":23461,"ProductType":"Credit"  ,"SubProduct": "CDS"  ,"RiskFactorType": "CSNQ"  ,"DeltaMargin":68586.28 ,"VegaMargin":-68545.95,"CurvatureMargin":51250.57,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"Mary", "DealID":23466,"ProductType":"Credit"  ,"SubProduct": "CDS"  ,"RiskFactorType": "CSNQ"  ,"DeltaMargin":68586.28 ,"VegaMargin":-68545.95,"CurvatureMargin":51250.57,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"Jhon", "DealID":23471,"ProductType":"Credit" ,"SubProduct":"Credit Options" ,            "RiskFactorType": "CSNQ" ,"DeltaMargin":91448.37, "VegaMargin":-91394.60, "CurvatureMargin":68334.09,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"Jhon", "DealID":23476,"ProductType":"Credit"  ,"SubProduct": "Credit Options" ,"RiskFactorType": "CSRNQ" ,"DeltaMargin":137172.56 ,"VegaMargin":-137091.90,"CurvatureMargin":102501.14,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"", "DealID":23481,"ProductType": "Credit" ,"SubProduct": "Credit Options" ,"RiskFactorType":"CSRNQ"  ,"DeltaMargin": 114310.47 ,"VegaMargin":-114243.25,"CurvatureMargin":85417.62,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"", "DealID":23486,"ProductType": "Credit" ,"SubProduct": "TRS"  ,"RiskFactorType": "CSRNQ" ,"DeltaMargin":68586.28 ,"VegaMargin":-68545.95,"CurvatureMargin":51250.57,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"", "DealID":23491,"ProductType": "Credit" ,"SubProduct": "TRS" ,"RiskFactorType": "CSRNQ" ,"DeltaMargin": 45724.19,"VegaMargin":-45697.30,"CurvatureMargin":34167.05,"TradeDate":'01-05-16' },
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":3456, "TradeName":"", "DealID":23496,"ProductType": "Credit" ,"SubProduct": "TRS"  ,"RiskFactorType": "CSRNQ" ,"DeltaMargin": 22862.09,"VegaMargin":-22848.65,"CurvatureMargin":17083.52,"TradeDate": '01-05-16'},
				
				{"CounterParty": "Total:"  ,"Poortfolio":"", "TradeName":"", "DealID":"","ProductType":""  ,"SubProduct":  "" ,"RiskFactorType": "" ,"DeltaMargin":685862.80,"VegaMargin":-685459.50,"CurvatureMargin":512505.70,"TradeDate": ''}
			
			];
    
            var creditTrades = [
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98565,"ProductType":"Equity"  ,"SubProduct": "Option"  ,"RiskFactorType": "EQ"  ,"DeltaMargin":28114.51,"VegaMargin":28079.56,"CurvatureMargin":23085.78,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98569,"ProductType": "Equity" ,"SubProduct": "Option" ,"RiskFactorType": "EQ" ,"DeltaMargin":84343.54,"VegaMargin":84238.69,"CurvatureMargin":69257.34,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98573,"ProductType":"Equity"  ,"SubProduct": "Option"  ,"RiskFactorType": "EQ"  ,"DeltaMargin":112458.50,"VegaMargin":112318.26,"CurvatureMargin":92343.12,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98577,"ProductType":"Equity"  ,"SubProduct": "Option"  ,"RiskFactorType": "EQ"  ,"DeltaMargin":168687.07,"VegaMargin":168477.38,"CurvatureMargin":138514.67,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98581,"ProductType":"Equity" ,"SubProduct":"Option" , "RiskFactorType": "EQ" ,"DeltaMargin":140572.56, "VegaMargin":140397.82, "CurvatureMargin":115428.90,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98585,"ProductType":"Equity"  ,"SubProduct": "Option" ,"RiskFactorType": "EQ" ,"DeltaMargin":196801.59,"VegaMargin":196556.95,"CurvatureMargin":161600.45,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98589,"ProductType": "Equity" ,"SubProduct": "Option" ,"RiskFactorType":"EQ"  ,"DeltaMargin":224916.10,"VegaMargin":224636.51,"CurvatureMargin":184686.23,"TradeDate": '01-05-16'},
				
				{"CounterParty": "JP Morgan"  ,"Poortfolio":2345, "TradeName":"Jack", "DealID":98593,"ProductType": "Equity" ,"SubProduct": "Option"  ,"RiskFactorType": "EQ" ,"DeltaMargin":28114.51,"VegaMargin":28079.56,"CurvatureMargin":23085.78,"TradeDate": '01-05-16'},
				
				
				
				{"CounterParty": "Total:"  ,"Poortfolio":"", "TradeName":"", "DealID":"","ProductType":""  ,"SubProduct":  "" ,"RiskFactorType": "" ,"DeltaMargin":984008.38,"VegaMargin":982784.73,"CurvatureMargin":808002.27,"TradeDate": ''}
				
			
			];
    
            var counterDuplies  = [
				{"RiskCategory":"Commodity", "CounterParty":"Citigroup"  ,"CSAID":"CITI01", "TradingDesk":"US Commodity", "Portfolio":4567 ,"Currency":"USD"  ,"DeltaMargin":	     		                  11251118.55,"VegaMargin":1248366.73,"CurvatureMargin":1073751.64,"Total":323253.64, "Trades":''},
				
				{"RiskCategory":"Credit", "CounterParty": "	JP Morgan"  ,"CSAID":"JPM02", "TradingDesk":"Prop trading", "Portfolio":3456 ,"Currency":"EUR"  ,"DeltaMargin":685862.81,	                 "VegaMargin":-685459.48,"CurvatureMargin":512505.69,"Total":323253.64, "Trades":''},
				
				{"RiskCategory":"Equity", "CounterParty": "	JP Morgan"  ,"CSAID":"JPM02", "TradingDesk":"Prop trading", "Portfolio":2345 ,"Currency":"USD"  ,"DeltaMargin":984007.93,	                 "VegaMargin":982784.74,"CurvatureMargin":808002.27,"Total":323253.64, "Trades":''},
				
				{"RiskCategory":"Rates And FX", "CounterParty": "	JP Morgan"  ,"CSAID":"JPM01", "TradingDesk":"FX Forward", "Portfolio":1234 ,"Currency":"EUR"  ,"DeltaMargin":	                  500253.11,"VegaMargin":500167.97,"CurvatureMargin":323253.64,"Total":323253.64, "Trades":''},
				
				{"RiskCategory":"Total:", "CounterParty": ""  ,"CSAID":"", "TradingDesk":"", "Portfolio":11602.00,"Currency":"0.00"  ,"DeltaMargin":13421242.40,"VegaMargin":"","CurvatureMargin":'',"Total":'', "Trades":''}
			
			];
    
        var simms = [
					{"SimulationDate":"Cell 7", "SimulationID":"Cell 8", "SimulationDescription":"Cell 9","Select":""},
					{"SimulationDate":"Cell 10", "SimulationID":"Cell 11", "SimulationDescription":"Cell 12","Select":""},
					{"SimulationDate":"1/2/2016", "SimulationID":"20160102-Production", "SimulationDescription":"EOD Run","Select":""},
					{"SimulationDate":"1/2/2016", "SimulationID":"20160102-IRS Shock", "SimulationDescription":"Teds Report","Select":""}
					
				
				];
    
    var portfoliosData = [
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1234 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1235 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1236 },
        {"CounterParty":"JP Morgan & Chase", "Portfolio":1237 },
        {"CounterParty":"Bank of America", "Portfolio":5555 },
        {"CounterParty":"Bank of America", "Portfolio":5557 },
        {"CounterParty":"Bank of America", "Portfolio":5558 },
        {"CounterParty":"Bank of America", "Portfolio":5559 }
        
    ];
    
    var branchRules = [
        {"CounterParty":"JP Morgan & Chase","Portfolio":1234,"SIMMThreshold":1000000},
        {"CounterParty":"Citigroup","Portfolio":1235,"SIMMThreshold":500000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1236,"SIMMThreshold":750000},
        {"CounterParty":"Barclays PLC","Portfolio":1237,"SIMMThreshold":125000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1238,"SIMMThreshold":750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1239,"SIMMThreshold":1750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1243,"SIMMThreshold":5750000},
        {"CounterParty":"JP Morgan & Chase","Portfolio":1241,"SIMMThreshold":5750000}
    ];
    
    //RiskCtrl table content
    
    var riskCtrlData = [
        {"CounterParty":"Citigroup","RiskCategory":"Commodity","Currency":"USD","DeltaMargin":11251118.55,"VegaMargin":1248366.73,"CurvatureMargin":1073751.64,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Rates and  FX","Currency":"EUR","DeltaMargin":500253.11,"VegaMargin":500167.97,"CurvatureMargin":323253.64,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Credit","Currency":"EUR","DeltaMargin":685862.81,"VegaMargin":-685459.48,"CurvatureMargin":512505.69,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Equity","Currency":"USD","DeltaMargin":984007.93,"VegaMargin":982784.74,"CurvatureMargin":808002.27	,"Total":323253.64},
        
         {"CounterParty":"Total:","RiskCategory":"Equity","Currency":"","DeltaMargin":13421242.40,"VegaMargin":2045859.96,"CurvatureMargin":2717513.24,"Total":1293014.56}
        
    ];
    
    var riskHeaders = [
        {"counter":"Counter Party","risk":"Risk Category","cur":"Currency","delta":"Delta Margin","Vega":"Vega Margin","curvarture":"Curvature Margin","total":"Total" }
    ];
    
    var simmActionHeader = [
        {"risk":"Risk  Type",  "qualifier":"Qualifier", "bucket":"Bucket", "label1":"Label1", "label2":"Label2", "amount":"Amount", "amountCurrency":"AmountCurrency", "amountUSD":"AmountUSD", "tradelD":"TradelD", "portfolioID":"PortfolioID"}
    ];
    
    //Simm Action Table Content
    var simmActionContent =[
        {"riskType":"Risk_Commodity", "qualifier":"Precious Metals Gold", "bucket":"12 (Precious Metals)", "label1":"", "label2":"", "amount":66124, "amountCurrency":"USD", "amountUSD":66124, "tradelD":121, "portfolioID":"Commodity_5455" },
        
        {"riskType":"Risk_CommodityVol", "qualifier":"Precious Metals Gold", "bucket":"12 (Precious Metals)", "label1":"3m", "label2":"", "amount":23754, "amountCurrency":"USD", "amountUSD":23754, "tradelD":343, "portfolioID":"Commodity_6767" },
        
        {"riskType":"Risk_CreditQ", "qualifier":"ISIN: XS1081333921", "bucket":"3 (Investment grade)", "label1":"5Y", "label2":"", "amount":4939, "amountCurrency":"USD", "amountUSD":4939, "tradelD":10, "portfolioID":"Credit_5678" },
        
        {"riskType":"Risk_Equity", "qualifier":"FTSE100", "bucket":"11 (Indexes,Funds,ETFs)", "label1":"", "label2":"", "amount":84498, "amountCurrency":"USD", "amountUSD":84498, "tradelD":110, "portfolioID":"Equity_4354" },
        
        {"riskType":"Risk_EquityVol", "qualifier":"FTSE100", "bucket":"11 (Indexes,Funds,ETFs)", "label1":"1y", "label2":"", "amount":59578, "amountCurrency":"USD", "amountUSD":59578, "tradelD":122, "portfolioID":"Equity_4354" },
        
        {"riskType":"Risk_FX", "qualifier":"EUR", "bucket":"", "label1":"", "label2":"", "amount":-92320, "amountCurrency":"USD", "amountUSD":-92320, "tradelD":122, "portfolioID":"RatesFX_4532" },
        
        {"riskType":"Risk_FX", "qualifier":"USD", "bucket":"", "label1":"", "label2":"", "amount":99765, "amountCurrency":"USD", "amountUSD":99765, "tradelD":1344, "portfolioID":"RatesFX_4532" },
        
        {"riskType":"Risk_FXVol", "qualifier":"USDJPY", "bucket":"", "label1":"3m", "label2":"", "amount":19768, "amountCurrency":"USD", "amountUSD":19768, "tradelD":2120, "portfolioID":"RatesFX_7373"},
        
        {"riskType":"Risk_Inflation", "qualifier":"USD", "bucket":"", "label1":"", "label2":"", "amount":-6968, "amountCurrency":"USD", "amountUSD":-6968, "tradelD":223, "portfolioID":"RatesFX_6676" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"3m", "label2":"Libor3m", "amount":252.6303142, "amountCurrency":"USD", "amountUSD":252.6303142, "tradelD":1, "portfolioID":"IR_1234"},
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"6m", "label2":"Libor3m", "amount":255.2378803, "amountCurrency":"USD", "amountUSD":255.2378803, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"1Y", "label2":"Libor3m", "amount":504.1483298, "amountCurrency":"USD", "amountUSD":504.1483298, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"2Y", "label2":"Libor3m", "amount":1006.325006, "amountCurrency":"USD", "amountUSD":1006.325006, "tradelD":1, "portfolioID":"IR_1234"},
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"3Y", "label2":"Libor3m", "amount":996.9373229	, "amountCurrency":"USD", "amountUSD":996.9373229, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"5Y", "label2":"Libor3m", "amount":1718.802164, "amountCurrency":"USD", "amountUSD":1718.802164, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"5y", "label2":"", "amount":-4881, "amountCurrency":"USD", "amountUSD":-4881, "tradelD":55, "portfolioID":"RatesFX_6676"},
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"1y", "label2":"", "amount":1640, "amountCurrency":"USD", "amountUSD":1640, "tradelD":3434, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"2y", "label2":"", "amount":97420, "amountCurrency":"USD", "amountUSD":97420, "tradelD":3435, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"3y", "label2":"", "amount":108502, "amountCurrency":"USD", "amountUSD":108502, "tradelD":3436, "portfolioID":"RatesFX_3422"},
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"5y", "label2":"", "amount":185665, "amountCurrency":"USD", "amountUSD":185665, "tradelD":3437, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"10y", "label2":"", "amount":76930, "amountCurrency":"USD", "amountUSD":76930, "tradelD":3438, "portfolioID":"RatesFX_3422" }
        
    ];
    
    //Blazers Header
    var blazerHeaders = [
        {"counterParty":"CounterParty","portfolio":"Portfolio","breach":"Breach"}
    ];
    
    //Blazers Content
    var blazerContent = [
        {"counterParty":"Barclays PLC","portfolio":"TIPS Portfolio","breach":"+1.1%"},
        {"counterParty":"Citigroup","portfolio":"FX EM Portfolio","breach":"+2%"},
        {"counterParty":"JP Morgan & Chase","portfolio":"Credit Trading","breach":"+3%"},
        {"counterParty":"JP Morgan & Chase","portfolio":"IRS Euro Area","breach":"+5%"}
    ];
    
    //Simmulation Portfolios data
    var portFolios = [
        {"lordP":"Bank of America North East Gas Portfolio"}
    ];
    
    //Simmulation LordCountries data
    var lordCountries = [
        {"lordC":"Bank of America"}
    ];
    
    // Simmulation trade content
    
    var tradeContents = [
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Rates and FX", "currency":"EUR", "deltaMargin":500253.11, "vegaMargin":500167.97, "curvatureMargin":323253.64, "Total":323253.64},
         
        {"counterParty":"JP Morgan & Chase","trade":1234, "riskCategory":"Credit", "currency":"EUR", "deltaMargin":685862.81, "vegaMargin":-685459.48, "curvatureMargin":512505.69, "Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Equity","currency":"USD","deltaMargin":	984007.93,"vegaMargin":982784.74,"curvatureMargin":808002.27,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Commodity","currency":"USD","deltaMargin":11251118.55,"vegaMargin":1248366.73,"curvatureMargin":1073751.64,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Rates and FX","currency":"EUR","deltaMargin":500253.11,"vegaMargin":500167.97,"curvatureMargin":323253.64,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Credit","currency":"EUR","deltaMargin":685862.81,"vegaMargin":-685459.48,"curvatureMargin":512505.69,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Equity","currency":"USD","deltaMargin":984007.93,"vegaMargin":982784.74,"curvatureMargin":808002.27,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Commodity","currency":"USD","deltaMargin":11251118.55,"vegaMargin":1248366.73,"curvatureMargin":1073751.64,"Total":323253.64},
        
        {"counterParty":"Total","trade":"","riskCategory":"","currency":"","deltaMargin":1251118.55,"vegaMargin":-1248366.73,"curvatureMargin":-1073751.64,"Total":323253.64}
    ];
    
    //SIMM Report Attr Header
    
    var simmAttrHeader = [
        {"cptyGrp":"Cpty Grp","cSAID":"CSA ID","riskCat":"Risk Cat","tradingDesk":"Trading Desk", "portfolio":"Portfolio","tradeID":"Trade ID","new":"New","marketOperations":"Market Operations","mrktDelta":"Mrkt Delta","total":"Total"  }
                            
    ];
    
    
    //SIMM Attr Content
    var simmAttrContent = [
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0001',"tradeID":100,"new":'0',"marketOperations":'0',"mrktDelta":'0',"total":100},
        
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0002',"tradeID":'0',"new":300,"marketOperations":'0',"mrktDelta":'0',"total":300},
        
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0003',"tradeID":'0',"new":'0',"marketOperations":-200,"mrktDelta":'0',"total":-200}
    ];
    
    // SIMM CSA Headers Content
    var csaHeadersContent = [
        {"delta":"Delta","vega":"Vega","curvature":"Curvature","total":"Total"}
    ];
    
    //SIMM CSA Body Content
    
    var csaBodyContent = [
        {"counterParty":"JPMorgan", "csaId":"JPM 01	","riskFactor":"GIRR & FX","riskBkt":"USD","concRisk":0.69,"aggSen":1.39,"sb":3.47,"rb":1.65,"kb":3.68,"sb1":16.38,"crb":4.05,"kb":8.43,"sb2":0.81,"crbKB":40.57},
        
        {"counterParty":"JPMorgan", "csaId":"JPM 01	","riskFactor":"GIRR & FX","riskBkt":"EUR","concRisk":7.64,"aggSen":77.25,"sb":109.49,"rb":16.13,"kb":147.96,"sb1":14.10,"crb":106.02,"kb":0.81,"sb2":9.08,"crbKB":488.48},
        
        {"counterParty":"JPMorgan", "csaId":"JPM 02","riskFactor":"Equity","riskBkt":"Developed Markets (Telecom. Industrial)","concRisk":35.64	,"aggSen":707.25,"sb":509.49,"rb":116.13,"kb":17.96,"sb1":18.10	,"crb":206.02,"kb":0.91,"sb2":91.08,"crbKB":1702.58}
    ];
    
    //SIMM Admin Table Headers
    var tableHeadings = [{"config":"Config","setting":"Setting"}];
    
    var tbl1Data = [
        {"config":"Server","setting":"LNPS101"},
        {"config":"Port","setting":12021},
        {"config":"Environment","setting":"Production"},
        {"config":"Type","setting":"Primary"}
    ];
    
    var tbl2Data = [
        {"config":"Server","setting":"ACPS101"},
        {"config":"Port","setting":12021},
        {"config":"Environment","setting":"Production"},
        {"config":"Type","setting":"Secondary"}
        
    ];
    
    var tbl3Data = [
        
        {"config":"Data","setting":"Prices"},
        {"config":"Server","setting":"XYZ101"},
        {"config":"Port","setting":5000},
        {"config":"Environment","setting":"Production"}
    ];
    
    var tbl4Data = [
        
        {"config":"Data","setting":"Sensitivities"},
        {"config":"Server","setting":"XYZ101"},
        {"config":"Port","setting":5000},
        {"config":"Environment","setting":"Production"}
    ];
    
    var statusTbl = [
        {"dataItem":"Data Item","status":"Status"}
    ];
    
    var statusTblData = [
         {"dataItem":"Trade Load","status":"COMPLETE"},
        {"dataItem":"LIBOR Fixings","status":"COMPLETE"},
        {"dataItem":"Bond Prices","status":"COMPLETE"},
        {"dataItem":"FX Prices","status":"UNAVAILABLE"},
        {"dataItem":"SIMM Calculation","status":"IN PROGRESS"}
    ];
    
    //Reference Data
    
    var curveTblTitle = [
        {"curveName":"Curve Name", "curveType":"Curve Type","curve":"Curve","tenor":"Tenor","discountFactor":"Discount Factor"}
    ];
    
    var curveTblData = [
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"0D", "discountFactor":0.001571524},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1M", "discountFactor":0.000934099},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2M", "discountFactor":0.000948444},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3M", "discountFactor":0.000935866},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6M", "discountFactor":0.001027672},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9M", "discountFactor":0.001280398},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1Y", "discountFactor":0.001841773},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2Y", "discountFactor":0.005475661},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3Y", "discountFactor":0.009655095},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"4Y", "discountFactor":0.013191876},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"5Y", "discountFactor":0.015783922},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6Y", "discountFactor":0.017845475},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"7Y", "discountFactor":0.019539006},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"8Y", "discountFactor":0.02095814},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9Y", "discountFactor":0.02217629},        
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"10Y", "discountFactor":0.023239034},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"12Y", "discountFactor":0.02501226},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"15Y", "discountFactor":0.026896017},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"20Y", "discountFactor":0.028543824},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"25Y", "discountFactor":0.029327868},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"30Y", "discountFactor":0.029693673},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"0D", "discountFactor":0.011571524},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1M", "discountFactor":0.010934099},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2M", "discountFactor":0.010948444},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3M", "discountFactor":0.010935866},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6M", "discountFactor":0.011027672},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9M", "discountFactor":0.011280398},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1Y", "discountFactor":0.011841773},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2Y", "discountFactor":0.015475661},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3Y", "discountFactor":0.019655095},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"4Y", "discountFactor":0.023191876},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"5Y", "discountFactor":0.025783922},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6Y", "discountFactor":0.027845475},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"7Y", "discountFactor":0.029539006},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"8Y", "discountFactor":0.03095814},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9Y", "discountFactor":0.03217629},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"10Y", "discountFactor":0.033239034},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"12Y", "discountFactor":0.03501226},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"15Y", "discountFactor":0.036896017},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"20Y", "discountFactor":0.038543824},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"25Y", "discountFactor":0.039327868},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"30Y", "discountFactor":0.039693673}
        
        
    ];
    
    
    
    var holidayHeading = [
        {"holidayDate":"Holiday Date", "description":"Holiday Description"}
    ];
    
    var holidayContent= [
         {"holidayDate":'2016-01-01', "description":"New Year Day"},
        {"holidayDate":'2016-01-18', "description":"Martin Luther King Jr.Day"},
        {"holidayDate":'2016-02-15', "description":"Presidents Day(Washingtons Birthday)"},
        {"holidayDate":'2016-05-30', "description":"Memorial Day"},
        {"holidayDate":'2016-07-04', "description":"Independance Day"},
        {"holidayDate":'2016-09-05', "description":"Labor Day"},
        {"holidayDate":'2016-10-10', "description":"Columbus Day"},
        {"holidayDate":'2016-11-11', "description":"Veterans Day"},
        {"holidayDate":'2016-11-24', "description":"Thanks giving Day"},
        {"holidayDate":'2016-12-25', "description":"Christmas Day"},
    ];
    
    var countryName = [
        {"name":"Select Country"},
        {"name":"USA"},
    ];
   
    var priceHeaders = [
    
        {"date":"Date","security":"Security","price":"Price"}
    ];
    
    var priceContents = [
        {"date":'04/10/2016', "security":"LIBOR 3M", "price":0.225},
         {"date":'04/10/2016', "security":"LIBOR 6M", "price":0.325},
         {"date":'04/10/2016', "security":"LIBOR 9M", "price":0.425},
         {"date":'04/10/2016', "security":"LIBOR 12M", "price":0.525},
         {"date":'04/10/2016', "security":"EDU6", "price":0.525},
         {"date":'04/10/2016', "security":"EDM6", "price":0.525}
    ];
    
    var generalTableHeaders = [
        
        {"riskClass":"Risk Class", "girr":"GIRR", "credit":"Credit (Qualifying)", "nonCredit":"Credit (Non-Qualifying)", "equity":"Equity", "commodity":"Commodity","fx":"FX"}
    ];
    
    var generalTableContents = [
        {"riskClass":"GIRR", "girr":1, "credit":"9%", "nonCredit":"10%", "equity":"18%", "commodity":"32%", "fx":"27%"},
        {"riskClass":"Credit (Qualifying)", "girr":"9%", "credit":1, "nonCredit":"24%", "equity":"58%", "commodity":"34%", "fx":"29%"},
        {"riskClass":"Credit (Non-Qualifying)", "girr":"10%", "credit":"24%", "nonCredit":1, "equity":"23%", "commodity":"24%", "fx":"12%"},
        {"riskClass":"Equity", "girr":"18%", "credit":"58%", "nonCredit":"23%", "equity":1, "commodity":"26%", "fx":"31%"},
        {"riskClass":"Commodity", "girr":"32%", "credit":"34%", "nonCredit":"24%", "equity":"26%", "commodity":1, "fx":"37%"},
        {"riskClass":"FX", "girr":"27%", "credit":"29%", "nonCredit":"12%", "equity":"31%", "commodity":"37%", "fx":1}
    ];
    
    var girrTbl1Content= [
        {"currencyGrp":"All Other Currencies", "concentration":"TBD"},
        {"currencyGrp":"G10+DKK", "concentration":"TBD"}
    ];
    
    var girrTbl2Content = [
        {"currency":"AUD", "volatility":"Regular Volatility"},
        {"currency":"CAD", "volatility":"Regular Volatility"},
        {"currency":"CHF", "volatility":"Regular Volatility"},
        {"currency":"DKK", "volatility":"Regular Volatility"},
        {"currency":"EUR", "volatility":"Regular Volatility"},
        {"currency":"GBP", "volatility":"Regular Volatility"},
        {"currency":"HKD", "volatility":"Regular Volatility"},
        {"currency":"JPY", "volatility":"Regular Volatility"},
        {"currency":"KRW", "volatility":"Regular Volatility"},
        {"currency":"NOK", "volatility":"Regular Volatility"},
        {"currency":"NZD", "volatility":"Regular Volatility"},
        {"currency":"SEK", "volatility":"Regular Volatility"},
        {"currency":"SGD", "volatility":"Regular Volatility"},
        {"currency":"TWD", "volatility":"Regular Volatility"},
        {"currency":"USD", "volatility":"Regular Volatility"},
    ];
    
    var girrTbl3Content =[
        {"bucket":"High Volatility Currencies", "one":89, "two":89, "three":89, "four":94, "five":104, "six":99, "seven":96, "eight":99, "nine":87, "ten":97, "eleven":97, "twele": 98},
        {"bucket":"Low Volatility Currencies", "one":10, "two":10, "three":10, "four":10, "five":13, "six":16, "seven":18, "eight":20, "nine":25, "ten":22, "eleven":22, "twele":23 },
        {"bucket":"Regular Currencies", "one":77, "two":77, "three":77, "four":64, "five":58, "six":49, "seven":47, "eight":47, "nine":45, "ten":45, "eleven":48, "twele":56 },
        {"bucket":"Tenor", "one":"2w", "two":"1m", "three":"3m", "four":"6m", "five":"1y", "six":"2y", "seven":"3y", "eight":"5y", "nine":"10y", "ten":"15y", "eleven":"20y", "twele":"30y" },
        
    ];
    
    var girrTbl4Content = [
        {"data":"k-10 (30yr)", "tm":0.129, "sm":0.296, "oy":0.471, "ty":0.602, "thy":0.69, "fy":0.812, "teny":0.931, "fify":0.97, "twey":0.988, "thiry":1},
         {"data":"k=1 (3m)", "tm":1, "sm":0.782, "oy":0.618, "ty":0.498, "thy":0.438, "fy":0.361, "teny":0.27, "fify":0.196, "twey":0.174, "thiry":0.129},
         {"data":"k=2 (6m)", "tm":0.782, "sm":1, "oy":0.618, "ty":0.739, "thy":0.438, "fy":0.569, "teny":0.444, "fify":0.375, "twey":0.349, "thiry":0.296},
         {"data":"k=3 (1yr)", "tm":0.618, "sm":0.84, "oy":1, "ty":0.917, "thy":0.859, "fy":0.757, "teny":0.626, "fify":0.555, "twey":0.526, "thiry":0.471},
         {"data":"k=4 (2yr)", "tm":0.498, "sm":0.739, "oy":0.917, "ty":1, "thy":0.976, "fy":0.895, "teny":0.749, "fify":0.69, "twey":0.66, "thiry":0.602},
         {"data":"k=5 (3yr)", "tm":0.438, "sm":0.667, "oy":0.859, "ty":0.976, "thy":1, "fy":0.958, "teny":0.831, "fify":0.779, "twey":0.746, "thiry":0.69},
         {"data":"k=6 (5yr)", "tm":0.361, "sm":0.569, "oy":0.757, "ty":0.895, "thy":0.958, "fy":1, "teny":0.925, "fify":0.893, "twey":0.859, "thiry":0.812},
         {"data":"k=7 (10yr)", "tm":0.27, "sm":0.444, "oy":0.626, "ty":0.749, "thy":0.831, "fy":0.925, "teny":1, "fify":0.98, "twey":0.961, "thiry":0.931},
         {"data":"k=8 (15yr)", "tm":0.196, "sm":0.375, "oy":0.555, "ty":0.69, "thy":0.779, "fy":0.893, "teny":0.98, "fify":1, "twey":0.989, "thiry":0.97},
         {"data":"k=9 (20yr)", "tm":0.174, "sm":0.349, "oy":0.526, "ty":0.66, "thy":0.746, "fy":0.859, "teny":0.961, "fify":0.989, "twey":1, "thiry":0.988}
    ];
    
    //Credit Qualification
    
    var credTbl1Content = [
        {"bucketNo":1, "crQuality":"Investment grade (IG)", "sector":"Sovereigns including central banks", "riskWeight":97},
        {"bucketNo":2, "crQuality":"Investment grade (IG)", "sector":"Financials including government-backed financials", "riskWeight":110},
        {"bucketNo":3, "crQuality":"Investment grade (IG)", "sector":"Basic materials energy industrials", "riskWeight":73},
        {"bucketNo":4, "crQuality":"Investment grade (IG)", "sector":"Consumer", "riskWeight":65},
        {"bucketNo":5, "crQuality":"Investment grade (IG)", "sector":"Technology telecommunications", "riskWeight":52},
        {"bucketNo":6, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Health care utilities local government government-backed corporates (non- financial)", "riskWeight":39},
        {"bucketNo":7, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Sovereigns including central banks", "riskWeight":198},
        {"bucketNo":8, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Financials including government backed financials", "riskWeight":638},
        {"bucketNo":9, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Basic materials energy industrials", "riskWeight":210},
        {"bucketNo":10, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Consumer", "riskWeight":375},
        {"bucketNo":11, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Technology telecommunications", "riskWeight":240},
        {"bucketNo":12, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"	Health care utilities local government government-backed corporates (non- financial)", "riskWeight":152},
        {"bucketNo":"Residual", "crQuality":"", "sector":"", "riskWeight":638},
    ];
    
    var credTbl2Content = [
        {"data":"Aggregated Sensitivities", "bucket":"98%", "riskWeight":"55%" },
         {"data":"Residual Bucket", "bucket":"50%", "riskWeight":"50%" }
    ];
    
    var credTbl3Content = [
        {"crRisk":"Qualifying", "concentration":"TBD" },
        {"crRisk":"Non-Qualifying", "concentration":"TBD" }
    ];
    
    var credTbl4Content = [
        {"bucket":"b=1", "c1":1, "c2":0.51, "c3":0.47, "c4":0.49, "c5":0.46, "c6":0.47, "c7":0.41, "c8":0.36, "c9":0.45, "c10":0.47, "c11":0.47, "c12": 0.43},
        {"bucket":"b=2", "c1":0.51, "c2":1, "c3":0.52, "c4":0.52, "c5":0.49, "c6":0.52, "c7":0.37, "c8":0.41, "c9":0.51, "c10":0.5, "c11":0.51, "c12": 0.46},
        {"bucket":"b=3", "c1":0.47, "c2":0.52, "c3":1, "c4":0.54, "c5":0.51, "c6":0.55, "c7":0.37, "c8":0.37, "c9":0.51, "c10":0.49, "c11":0.5, "c12":0.47 },
        {"bucket":"b=4", "c1":0.49, "c2":0.52, "c3":0.54, "c4":1, "c5":0.53, "c6":0.56, "c7":0.36, "c8":0.37, "c9":0.52, "c10":0.51, "c11":0.51, "c12":0.46 },
        {"bucket":"b=5", "c1":0.46, "c2":0.49, "c3":0.51, "c4":0.53, "c5":1, "c6":0.54, "c7":0.35, "c8":0.35, "c9":0.49, "c10":0.48, "c11":0.5, "c12": 0.44},
        {"bucket":"b=6", "c1":0.47, "c2":0.52, "c3":0.55, "c4":0.56, "c5":0.54, "c6":1, "c7":0.37, "c8":0.37, "c9":0.52, "c10":0.49, "c11":0.51, "c12": 0.48},
        {"bucket":"b=7", "c1":0.41, "c2":0.37, "c3":0.37, "c4":0.36, "c5":0.35, "c6":0.37, "c7":1, "c8":0.29, "c9":0.36, "c10":0.52, "c11":0.36, "c12":0.36 },
        {"bucket":"b=8", "c1":0.36, "c2":0.41, "c3":37, "c4":0.37, "c5":0.35, "c6":0.37, "c7":0.29, "c8":1, "c9":0.37, "c10":0.36, "c11":0.37, "c12": 0.33},
        {"bucket":"b=9", "c1":0.45, "c2":0.51, "c3":0.51, "c4":0.52, "c5":0.49, "c6":0.52, "c3":0.36, "c8":0.37, "c9":1, "c10":0.49, "c11":0.5, "c12":0.46 },
        {"bucket":"b=10", "c1":0.47, "c2":0.5, "c3":0.49, "c4":0.51, "c5":0.48, "c6":0.49, "c7":0.34, "c8":0.36, "c9":0.49, "c10":1, "c11":0.49, "c12": 0.46},
        {"bucket":"b=11", "c1":0.47, "c2":0.51, "c3":0.5, "c4":0.51, "c5":0.5, "c6":0.51, "c7":0.36, "c8":0.37, "c9":0.5, "c10":0.49, "c11":1, "c12": 0.46},
        {"bucket":"b=12", "c1":0.43, "c2":0.46, "c3":0.47, "c4":0.46, "c5":0.44, "c6":0.48, "c7":0.36, "c8":0.33, "c9":0.46, "c10":0.46, "c11":0.46, "c12":1 }
    ];
   
    var nonCreditTbl1Content = [
        {"bktNo":1, "creditQlt":"Investment Grade (IG)", "sector":"RMBS/CMBS"},
         {"bktNo":2, "creditQlt":"High	Yield(HY) and Not Rated(NR)", "sector":"RMBS/CMBS"},
         {"bktNo":"Residual", "creditQlt":"", "sector":""}
    ];
    
    var nonCreditTbl2Content = [
        {"data":"regated Sensitivities", "bucket":"60%", "riskWeight":"21%"},
        {"data":"Residual Bucket", "bucket":"50%", "riskWeight":"50%"}
    ];
    
    var nonCreditTbl3Content = [
        {"bucket":1, "riskWeight":169},
        {"bucket":2, "riskWeight":1646},
         {"bucket":"Residual", "riskWeight":1646}
    ];
    
    //Commodity
    var comodity1Content = [
        {"bktNo":1, "commodity":"Coal", "riskWeight":9, "riskCorr":"71%", "concentration": "TBA A"},
        {"bktNo":2, "commodity":"Crude", "riskWeight":19, "riskCorr":"92%", "concentration":"TBA A" },
        {"bktNo":3, "commodity":"Light Ends", "riskWeight":18, "riskCorr":"97%", "concentration":"TBA A" },
        {"bktNo":4, "commodity":"Middle Distillates", "riskWeight":13, "riskCorr":"97%", "concentration":"TBA A" },
        {"bktNo":5, "commodity":"Heavy Distillates", "riskWeight":24, "riskCorr":"99%", "concentration":"TBA A" },
        {"bktNo":6, "commodity":"North America Natural Gas", "riskWeight":17, "riskCorr":"98%", "concentration":"TBA A" },
        {"bktNo":7, "commodity":"European Natural Gas", "riskWeight":21, "riskCorr":"100%", "concentration": "TBA A"},
        {"bktNo":8, "commodity":"North American Power", "riskWeight":35, "riskCorr":"69%", "concentration": "TBA B"},
        {"bktNo":9, "commodity":"European Power", "riskWeight":20, "riskCorr":"47%", "concentration":"TBA B" },
        {"bktNo":10, "commodity":"Freight", "riskWeight":50, "riskCorr":"1%", "concentration": "TBA B"},
        {"bktNo":11, "commodity":"Base Metals", "riskWeight":21, "riskCorr":"67%", "concentration": "TBA C"},
        {"bktNo":12, "commodity":"Precious Metals", "riskWeight":19, "riskCorr":"70%", "concentration": "TBA C"},
        {"bktNo":13, "commodity":"Grains", "riskWeight":17, "riskCorr":"68%", "concentration": "TBA C"},
        {"bktNo":14, "commodity":"Softs", "riskWeight":15, "riskCorr":"22%", "concentration": "TBA C"},
        {"bktNo":15, "commodity":"Livestock", "riskWeight":8, "riskCorr":"50%", "concentration": "TBA C"},
        {"bktNo":16, "commodity":"Other", "riskWeight":50, "riskCorr":"0%", "concentration": "TBA C"}
    ];
    
    var comodity2Content = [
        {"bucket":"b=1", "c1":"1%", "c2":"11%", "c3":"16%", "c4":"13%", "c5":"10%", "c6":"6%", "c7":"20%", "c8":"5%", "c9":"17%", "c10":"3%", "c11":"18%", "c12":"9%", "c13":"10%", "c14":"5%", "c15":"4%", "c16":"0%" },
        
         {"bucket":"b=10", "c1":"3%", "c2":"14%", "c3":"12%", "c4":"8%", "c5":"21%", "c6":"15%", "c7":"20%", "c8":"12%", "c9":"12%", "c10":"1%", "c11":"10", "c12":"7%", "c13":"9%", "c14":"10%", "c15":"16%", "c16":"0%" },
        
        {"bucket":"b=11", "c1":"10%", "c2":"30%", "c3":"32%", "c4":"31%", "c5":"34%", "c6":"0%", "c7":"6%", "c8":"-1%", "c9":"10%", "c10":"10%", "c11":"1%", "c12":"46%", "c13":"20%", "c14":"26%", "c15":"18%", "c16":"0%" },
        
        {"bucket":"b=12", "c1":"9%", "c2":"31%", "c3":"26%", "c4":"25%", "c5":"32%", "c6":"0%", "c7":"6%", "c8":"0%", "c9":"6%", "c10":"7%", "c11":"46%", "c12":"1%", "c13":"25%", "c14":"23%", "c15":"14%", "c16":"0%" },
        
        {"bucket":"b=13", "c1":"10%", "c2":"26%", "c3":"16%", "c4":"15%", "c5":"27%", "c6":"23%", "c7":"12%", "c8":"18%", "c9":"12%", "c10":"9%", "c11":"20%", "c12":"25%", "c13":"1%", "c14":"29%", "c15":"6%", "c16":"0%" },
        
        {"bucket":"b=14", "c1":"5%", "c2":"26%", "c3":"12%", "c4":"20%", "c5":"29%", "c6":"15%", "c7":"9%", "c8":"11%", "c9":"10%", "c10":"10%", "c11":"26%", "c12":"23%", "c13":"29%", "c14":"1%", "c15":"15%", "c16":"0%" },
        
        {"bucket":"b=15", "c1":"4%", "c2":"12%", "c3":"22%", "c4":"9%", "c5":"12%", "c6":"7%", "c7":"9%", "c8":"4%", "c9":"10%", "c10":"16%", "c11":"18%", "c12":"14%", "c13":"6%", "c14":"15%", "c15":"1%", "c16":"0%" },
        
        {"bucket":"b=16", "c1":"0%", "c2":"0%", "c3":"0%", "c4":"0%", "c5":"0%", "c6":"0%", "c7":"0%", "c8":"0%", "c9":"0%", "c10":"0%", "c11":"0%", "c12":"0%", "c13":"0%", "c14":"0%", "c15":"0%", "c16":"1%" },
        
        {"bucket":"b=2", "c1":"11%", "c2":"1%", "c3":"95%", "c4":"95%", "c5":"93%", "c6":"15%", "c7":"27%", "c8":"19%", "c9":"20%", "c10":"14%", "c11":"30%", "c12":"31%", "c13":"26%", "c14":"26%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=3", "c1":"16%", "c2":"95%", "c3":"1%", "c4":"92%", "c5":"90%", "c6":"17%", "c7":"24%", "c8":"14%", "c9":"17%", "c10":"12%", "c11":"32%", "c12":"26%", "c13":"16%", "c14":"22%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=4", "c1":"13%", "c2":"95%", "c3":"92%", "c4":"1%", "c5":"90%", "c6":"18%", "c7":"26%", "c8":"8%", "c9":"17%", "c10":"8%", "c11":"31%", "c12":"25%", "c13":"15%", "c14":"20%", "c15":"9%", "c16":"0%" },
        
        {"bucket":"b=5", "c1":"10%", "c2":"93%", "c3":"90%", "c4":"90%", "c5":"1%", "c6":"18%", "c7":"37%", "c8":"13%", "c9":"30%", "c10":"21%", "c11":"34%", "c12":"32%", "c13":"27%", "c14":"29%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=6", "c1":"6%", "c2":"15%", "c3":"17%", "c4":"80%", "c5":"18%", "c6":"1%", "c7":"7%", "c8":"62%", "c9":"3%", "c10":"15%", "c11":"0%", "c12":"0%", "c13":"23%", "c14":"15%", "c15":"7%", "c16": "0%"},
        
        {"bucket":"b=7", "c1":"20%", "c2":"27%", "c3":"24%", "c4":"26%", "c5":"37%", "c6":"7%", "c7":"1%", "c8":"7%", "c9":"66%", "c10":"20%", "c11":"6%", "c12":"6%", "c13":"12%", "c14":"9%", "c15":"9%", "c16":"0%" },
        
        {"bucket":"b=8", "c1":"5%", "c2":"19%", "c3":"14%", "c4":"90%", "c5":"13%", "c6":"62%", "c7":"7%", "c8":"1%", "c9":"9%", "c10":"12%", "c11":"97%", "c12":"0%", "c13":"18%", "c14":"11%", "c15":"4%", "c16":"0%" },
        
        {"bucket":"b=9", "c1":"17%", "c2":"20%", "c3":"17%", "c4":"8%", "c5":"30%", "c6":"3%", "c7":"66%", "c8":"9%", "c9":"1%", "c10":"12%", "c11":"10%", "c12":"6%", "c13":"12%", "c14":"10%", "c15":"10%", "c16":"0%" }
        
       
    ];
    
    var comodity3Content = [
        {"bktNo":1, "size":"Large", "region":"Emerging markets", "riskWeight":22, "riskCorre":"14%", "sector":'"Consumer goods and services transportation and storage administrative and support service activities utilities"'},
        {"bktNo":2, "size":"Large", "region":"Emerging markets", "riskWeight":28, "riskCorre":"24%", "sector":'"Telecommunications industrials"'},        
        {"bktNo":3, "size":"Large", "region":"Emerging markets", "riskWeight":28, "riskCorre":"25%", "sector":"Basic materials energy agriculture manufacturing mining and quarrying"},
        {"bktNo":4, "size":"Large", "region":"Emerging markets", "riskWeight":25, "riskCorre":"20%", "sector":'	"Financials including gov’t-backed financials real estate activities technology"'},
        {"bktNo":5, "size":"Large", "region":"Developed markets", "riskWeight":18, "riskCorre":"26%", "sector":"Consumer goods and services transportation and storage administrative and support service activities utilities"},
        {"bktNo":6, "size":"Large", "region":"Developed markets", "riskWeight":20, "riskCorre":"34%", "sector":"Telecommunications industrials"},
        {"bktNo":7, "size":"Large", "region":"Developed markets", "riskWeight":24, "riskCorre":"33%", "sector":"Basic materials energy agriculture manufacturing mining and quarrying"},
        {"bktNo":8, "size":"Large", "region":"Developed markets", "riskWeight":23, "riskCorre":"34%", "sector":"Financials including gov’t-backed financials real estate activities technology"},
        {"bktNo":9, "size":"Small", "region":"Emerging markets", "riskWeight":26, "riskCorre":"21%", "sector":"All sectors"},
        {"bktNo":10, "size":"Small", "region":"Developed markets", "riskWeight":27, "riskCorre":"24%", "sector":"All sectors"},
        {"bktNo":11, "size":"All", "region":"All", "riskWeight":15, "riskCorre":"63%", "sector":"Indexes Funds ETFs"},
        {"bktNo":12, "size":"residual", "region":"Developed markets", "riskWeight":28, "riskCorre":"0%", "sector":0}
        
    ];
    
    var comodity4Content = [
        {"equityRisk":"Developed Markets", "concentration":"TBD"},
        {"equityRisk":"Emerging Markets", "concentration":"TBD"},
        {"equityRisk":"Indexes Funds ETFs", "concentration":"TBD"}
        
    ];
    
    var comodity5Content = [
        {"bucket":"b=1", "c1":1, "c2":0.17, "c3":0.18, "c4":0.18, "c5":0.8, "c6":0.1, "c7":0.1, "c8":0.11, "c9":0.16, "c10":0.8, "c11":0.18},
         {"bucket":"b=2", "c1":0.17, "c2":1, "c3":0.24, "c4":0.19, "c5":0.7, "c6":0.1, "c7":0.9, "c8":0.1, "c9":0.19, "c10":0.7, "c11":0.18},
        {"bucket":"b=3", "c1":0.18, "c2":0.24, "c3":1, "c4":0.21, "c5":0.9, "c6":0.12, "c7":0.13, "c8":0.13, "c9":0.2, "c10":0.1, "c11":0.24},
        {"bucket":"b=4", "c1":0.16, "c2":0.19, "c3":0.21, "c4":1, "c5":0.13, "c6":0.17, "c7":0.16, "c8":0.17, "c9":0.2, "c10":0.13, "c11":0.3},
        {"bucket":"b=5", "c1":0.8, "c2":0.7, "c3":0.9, "c4":0.13, "c5":1, "c6":0.28, "c7":0.24, "c8":0.28, "c9":0.1, "c10":0.23, "c11":0.38},
        {"bucket":"b=6", "c1":0.1, "c2":0.1, "c3":0.12, "c4":0.17, "c5":0.28, "c6":1, "c7":0.3, "c8":0.33, "c9":0.13, "c10":0.26, "c11":0.45},
        {"bucket":"b=7", "c1":0.1, "c2":0.9, "c3":0.13, "c4":0.16, "c5":0.24, "c6":0.3, "c7":1, "c8":0.29, "c9":0.13, "c10":0.25, "c11":0.42},
        {"bucket":"b=8", "c1":0.11, "c2":0.1, "c3":0.13, "c4":0.17, "c5":0.28, "c6":0.33, "c7":0.29, "c8":1, "c9":0.14, "c10":0.27, "c11":0.45},
        {"bucket":"b=9", "c1":0.16, "c2":0.19, "c3":0.2, "c4":0.2, "c5":0.1, "c6":0.13, "c7":0.13, "c8":0.14, "c9":1, "c10":0.11, "c11":0.25},
        {"bucket":"b=10", "c1":0.8, "c2":0.7, "c3":0.1, "c4":0.13, "c5":0.23, "c6":0.26, "c7":0.25, "c8":0.27, "c9":0.11, "c10":1, "c11":0.34},
        {"bucket":"b=11", "c1":0.18, "c2":0.18, "c3":0.24, "c4":0.3, "c5":0.38, "c6":0.45, "c7":0.42, "c8":0.45, "c9":0.25, "c10":0.34, "c11":1},
        
    ];
    
    var fxsContent = [
        {"fxRisk":"All Currencies", "concentration":"TBD"}
    ];
    
    var selectBoxData = [	
					{"number":10},
					{"number":25},
					{"number":50},
					{"number":100}
				];
   
			

	angular.module('myApp.memoryServices',[])
		// Dashboard Module starts	
        .factory('prouctsList',
				function(){
										
				return{
						query: function()
						{	
							return companyDetails;							
						},
                        tableHead: function(){
                            return dashboardHeaders;
                        },
                        selectBox: function()
                        {
                            return selectBoxData;
                        }
				
					}
			}) // Dashboard Module Ends
            
    
        // Counter Party 2 Module starts	
            .factory('riskCategory',function(){
                return{
                    
                    query: function(){
                        return productsList;
                    },
                    tableHead: function()
                    {
                        return counter2Headers;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
            
            }) // Counter Party 2 Module Ends
           
            //Counter Party 1 Module Starts
            .factory('SIMMDetails',function(){
                return{
                    query: function()
                    {
                        return Counter1ProductList;
                    },
                    tableHead: function()
                    {
                        return counter2Headers;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                    
                }
                   
             })
            //Counter Party 1 Module Ends
            
            //Commodity Module Starts
            
            .factory('commodityTrade',function(){
                return{
                    query: function(){
                        return commodities;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
        
            })
            //Commodity Module Ends
    
            //Credit Traders start
                
            .factory('creditTrades',function(){
                return{
                    query: function(){
                        return creditTrades;
                    },
                     selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
        
            })
            //Credit Traders End
            
            //Equity Traders Start
    
            .factory('equityTrades',function(){
        
                return{
                    query: function(){
                        return creditTrades;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
            })
    
            //Equity Traders End
            
            //Counterparty Dupil Start
            
            .factory('counterPartyDupil', function(){
                return{
                    query:function()
                    {
                        return counterDuplies;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
            
            })
            //Counterpaty Dupil Ends
            
            //Simmulation Starts
            
            .factory('simmulationData',function(){
                return{
                    query: function()
                    {
                        return simms;
                    },
                    lordPortfolios: function()
                    {
                        return portFolios;
                    },
                    lordCountries: function()
                    {
                        return lordCountries;
                    },
                    loadHeaders: function()
                    {
                        return riskHeaders;
                    },
                    tradeContent: function()
                    {
                        return tradeContents;
                    }
                }
        
        
            })
            //Simmulation Ends
    
            //Personal Control Starts
    
            .factory('personControl',function(){
        
                return{
                    query: function()
                    {
                        return portfoliosData;
                    },
                    branchRul: function()
                    {
                        return branchRules;
                    }
                }
            })
            //Personal Control Ends
    
            // Risk Control Starts
            
            .factory('riskControl',function(){
                return{
                    query: function()
                    {
                        return riskCtrlData;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    },
                    riskHeader: function()
                    {
                        return riskHeaders;
                    }
                }
        
            })
            //Risk Control Ends
            
            //Custome Control Starts
            .factory('customeControl',function(){
                return{
                    custHeader: function()
                    {
                        return riskHeaders;
                    },
                    query: function()
                    {
                        return riskCtrlData;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
        
            })
            //Custome Control Ends
    
            //SIMM View starts
                
            .factory('simmView',function(){
                return{
                    custHeader: function()
                    {
                        return simmActionHeader;
                    },
                    query: function()
                    {
                        return simmActionContent;
                    },
                    blazerHeader: function()
                    {
                        return blazerHeaders;
                    },
                    queryBlazer: function()
                    {
                        return blazerContent;
                    }
                }
            })
            //SIMM View Ends
    
            //SIMM Reports Start
    
            .factory('simmReport',function(){
                return{
                    attrHeader: function()
                    {
                        return simmAttrHeader;
                    },
                    query: function()
                    {
                        return simmAttrContent;
                    },
                    csaHeaders: function()
                    {
                        return csaHeadersContent;
                    },
                    csaContent: function()
                    {
                        return csaBodyContent;
                    }
                }
                
            })
            //SIMM Reports End
    
            //SIMM Admin Starts
            .factory('simmAdmin',function(){
                return{
                    tblHeadings: function()
                    {
                        return tableHeadings;
                    },
                    tbl1: function()
                    {
                        return tbl1Data;
                    },
                    tbl2: function()
                    {
                        return tbl2Data;
                    },
                    tbl3: function()
                    {
                        return tbl3Data;
                    },
                    tbl4 : function()
                    {
                        return tbl4Data;
                    },
                    statusTblHeading: function()
                    {
                        return statusTbl;
                    },
                    statusTblContent: function()
                    {
                        return statusTblData;
                    }
                }
        
        
            })
            //SIMM Admin Ends
    
            .factory('refData',function(){
                return{
                    curvTitle: function()
                    {
                        return curveTblTitle;
                    },
                    curvData: function()
                    {
                        return curveTblData;
                    },
                    country: function()
                    {
                        return countryName;
                    },
                    holidayHeading: function()
                    {
                        return holidayHeading;
                    },
                    holidayList: function()
                    {
                        return holidayContent;
                    },
                    priceHeader: function()
                    {
                        return priceHeaders;
                    },
                    priceContent: function()
                    {
                        return priceContents;
                    },
                    generalTableHeader: function()
                    {
                        return generalTableHeaders;
                    },
                    generalTableContent: function()
                    {
                        return generalTableContents;
                    },
                    girrTbl1: function()
                    {
                        return girrTbl1Content;
                    },
                    girrTbl2: function()
                    {
                        return girrTbl2Content;
                    },
                    girrTbl3: function()
                    {
                        return girrTbl3Content;
                    },
                    girrTbl4: function()
                    {
                        return girrTbl4Content;
                    },
                    credTbl1: function()
                    {
                        return credTbl1Content;
                    },
                    credTbl2: function()
                    {
                        return credTbl2Content;
                    },
                    credTbl3: function()
                    {
                        return credTbl3Content;
                    },
                    credTbl4: function()
                    {
                        return credTbl4Content;
                    },
                    nonCreditTbl1: function()
                    {
                        return nonCreditTbl1Content;
                    },
                    nonCreditTbl2: function()
                    {
                        return nonCreditTbl2Content;
                    },
                    nonCreditTbl3: function()
                    {
                        return nonCreditTbl3Content;
                    },
                    nonCreditTbl4: function()
                    {
                        return credTbl3Content;
                    },
                    comodity1: function()
                    {
                        return comodity1Content;
                    },
                    comodity2: function()
                    {
                        return comodity2Content;
                    },
                    comodity3: function()
                    {
                        return comodity3Content;
                    },
                    comodity4: function()
                    {
                        return comodity4Content;
                    },
                    comodity5: function()
                    {
                        return comodity5Content;
                    },
                    fxs: function()
                    {
                        return fxsContent;
                    }
                }
                
            });
	
	
}());