angular.module('myApp.regular',[])
        .controller('regularController',['$scope','$rootScope','riskControl',
                function($scope,$rootScope,riskControl){
                
                $scope.title = "SIMM Views: SIMM by Risk Category";
                
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                    
                $scope.riskCategories = riskControl.query();
                $scope.arrLength = $scope.riskCategories.length;
                $scope.entries = riskControl.selectBox(); 
                $scope.riskHeaders = riskControl.riskHeader(); 
                    
                
                
            }])