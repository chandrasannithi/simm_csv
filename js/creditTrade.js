

(function(){
    
    var creditTradeController = function($scope,creditTradeSVC,$rootScope,creditTrades){
        $scope.title = "SIMM Views : Credit Trade Details";
        $scope.entries = creditTrades.selectBox();
        if($rootScope.simmulation)
        {
           $scope.mode = $rootScope.simmulation;
        }
        else
        {
            $scope.mode = "20160102-Production";   
        }
        
        
        creditTradeSVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                CounterParty:data[0],
                                Poortfolio: data[1],
                                TradeName: data[2],
                                DealID: data[3],
                                ProductType:data[4],
                                SubProduct: data[5],
                                RiskFactorType: data[6],
                                DeltaMargin:data[7],   
                                VegaMargin:data[8],   
                                CurvatureMargin:data[9],   
                                TradeDate:data[10],   
                            } 
                           
                           
                        }
                        lines.push(tarr);
                 
                }
                $scope.data = lines;
             ;
            
            
        });
        
    }
    
    
    angular.module('myApp.creditTrade',[])
    .controller('creditTradeController', ['$scope','creditTradeSVC','$rootScope','creditTrades',creditTradeController])
})();