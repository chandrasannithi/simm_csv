
 
 angular.module("myApp",['ngRoute',
 						
						
                         
                         //Dash board Controller
                         'myApp.dashboard',
                         
                         //Counterparty2
                         'myApp.counter2',
                         
                         //Counterparty1
                         'myApp.counter1',
                         
                         //Commodity-trade Controller 
                         'myApp.commodity',
                         
                         // Credit Traders Controller
                         'myApp.creditTrade',
                         
                          //Equidity Trade Controller
                         'myApp.equityTrade',
                         
                         //Counterparty dupil controller
                         'myApp.counterDupil',
                         
                          //Simmulation Controller
                         'myApp.simmulation',
                         
                        //PersonalWorkSpace Controller
                         'myApp.persoanlWorkSpace',
                         
                        // Regular Controller
                         'myApp.regular',
                         
                        //Cutsome Controller
                         'myApp.custom',
                         
                          //SIMM Actions Controller
                         'myApp.simmActions',
                         
                         //SIMM Report Controller
                         'myApp.simmReport',
                         
                          //SIMM Admin Controller
                         'myApp.simmAdmin',
                         
                         // Reference Data Views controler
                         'myApp.refData',
                         
                         //MemoryService
                         //Dashboard
                         'myApp.dashboardMS',
                         
                         //Counter2
                         'myApp.counter2MS',
                         
                         //Counter1
                         'myApp.counter1MS',
                         
                         //Commodities
                         'myApp.commodityMS',
                         
                         //CreditTrade
                         'myApp.creditTradeMS',
                         
                         //Equity Trade
                         'myApp.equityTradeMS',
                         
                         //Counter Dupil
                         'myApp.counterDupilMS',
                         
                         //Simmulations
                         'myApp.simmulationMS',
                         
                         //Personal WorkSpace
                         'myApp.personalMS',
                         
                         //RiskControl
                         'myApp.riskCtrlMS',
                         
                         //Custom
                         'myApp.customMS',
                         
                         //SIMM Actions
                         'myApp.simmActionsMS',
                         
                         //SIMM Report
                         'myApp.simmReportMS',
                         
                         //SIMM Admin
                         'myApp.simmAdminMS',
                         
                         //Refrence Data
                         'myApp.referenceDataMS',
                         
                         
                        // 'myApp.DashboardData'
                         
                         
						])	
		
			
			.config(['$routeProvider', function($routeProvider) {
   				$routeProvider.
   
			   when('/dashboard', {
			       templateUrl: 'partials/dashboard.html', controller: 'dashboardController'
			   }).
			   when('/counterparty2', {
				  templateUrl: 'partials/counterparty2.htm', controller: 'counterparty2Controller'
			   }).
               when('/counterparty1', {
				  templateUrl: 'partials/counterparty1.html', controller: 'counterparty1Controller'
			   }).
               when('/commodity', {
				  templateUrl: 'partials/commodity_trade.html', controller: 'commodityController'
			   }).
               when('/creditTrade', {
				  templateUrl: 'partials/credit_trade.html', controller: 'creditTradeController'
			   }).
               when('/equityTrade', {
				  templateUrl: 'partials/equity_trade.html', controller: 'equityTradeController'
			   }).
               when('/counterpartyDupli', {
				  templateUrl: 'partials/counterparty_dupli.html', controller: 'counterpartyDupliController'
			   }).
               when('/simmulation', {
				  templateUrl: 'partials/simmulation.html', controller: 'simmulationController'
			   }).
               when('/personalWorkSpace', {
				  templateUrl: 'partials/personalWorkSpace.html', controller: 'personalController'
			   }).
               when('/regular', {
				  templateUrl: 'partials/regular.html', controller: 'regularController'
			   }).
               when('/custom', {
				  templateUrl: 'partials/custom.html', controller: 'customController'
			   }).
               when('/SIMM_Actions', {
				  templateUrl: 'partials/SIMM_Actions.html', controller: 'SIMM_ActionsController'
			   }).
               when('/SIMM_Reports',{
                    templateUrl:'partials/SIMM_Reports.html', controller:'SIMM_ReportsController'
                }).
               when('/reference_data_view',{
                    templateUrl:'partials/reference_data_view.html', controller:'reference_data_viewController'
                }).
               when('/SIMM_Admin',{
                    templateUrl:'partials/SIMM_Admin.html', controller:'SIMM_AdminController'
                }). 
               otherwise({
                    redirectTo: '/dashboard'
                });
                
			 
               
   
	
			}]);
			
			
			
			
		
				
