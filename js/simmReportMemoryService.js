angular.module('myApp.simmReportMS',[])

     //SIMM Reports Start
    
            .factory('simmReport',function(){
                return{
                    attrHeader: function()
                    {
                        return simmAttrHeader;
                    },
                    query: function()
                    {
                        return simmAttrContent;
                    },
                    csaHeaders: function()
                    {
                        return csaHeadersContent;
                    },
                    csaContent: function()
                    {
                        return csaBodyContent;
                    }
                }
                
            })
            //SIMM Reports End


 //SIMM Report Attr Header
    
    var simmAttrHeader = [
        {"cptyGrp":"Cpty Grp","cSAID":"CSA ID","riskCat":"Risk Cat","tradingDesk":"Trading Desk", "portfolio":"Portfolio","tradeID":"Trade ID","new":"New","marketOperations":"Market Operations","mrktDelta":"Mrkt Delta","total":"Total"  }
                            
    ];

 //SIMM Attr Content
    var simmAttrContent = [
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0001',"tradeID":100,"new":'0',"marketOperations":'0',"mrktDelta":'0',"total":100},
        
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0002',"tradeID":'0',"new":300,"marketOperations":'0',"mrktDelta":'0',"total":300},
        
        {"cptyGrp":"JP Morgan","cSAID":"JPM 01","riskCat":"FX Forward","tradingDesk":1234,"portfolio":'0003',"tradeID":'0',"new":'0',"marketOperations":-200,"mrktDelta":'0',"total":-200}
    ];


 var csaHeadersContent = [
        {"delta":"Delta","vega":"Vega","curvature":"Curvature","total":"Total"}
    ];


//SIMM CSA Body Content
    
    var csaBodyContent = [
        {"counterParty":"JPMorgan", "csaId":"JPM 01	","riskFactor":"GIRR & FX","riskBkt":"USD","concRisk":0.69,"aggSen":1.39,"sb":3.47,"rb":1.65,"kb":3.68,"sb1":16.38,"crb":4.05,"kb":8.43,"sb2":0.81,"crbKB":40.57},
        
        {"counterParty":"JPMorgan", "csaId":"JPM 01	","riskFactor":"GIRR & FX","riskBkt":"EUR","concRisk":7.64,"aggSen":77.25,"sb":109.49,"rb":16.13,"kb":147.96,"sb1":14.10,"crb":106.02,"kb":0.81,"sb2":9.08,"crbKB":488.48},
        
        {"counterParty":"JPMorgan", "csaId":"JPM 02","riskFactor":"Equity","riskBkt":"Developed Markets (Telecom. Industrial)","concRisk":35.64	,"aggSen":707.25,"sb":509.49,"rb":116.13,"kb":17.96,"sb1":18.10	,"crb":206.02,"kb":0.91,"sb2":91.08,"crbKB":1702.58}
    ];