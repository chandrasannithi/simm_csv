angular.module('myApp.dashboardMS',[])
         .factory('prouctsList',
				function(){
										
				return{
						query: function()
						{	
							return companyDetails;							
						},
                        tableHead: function(){
                            return dashboardHeaders;
                        },
                        selectBox: function()
                        {
                            return selectBoxData;
                        }
				
					}
			})

  var dashboardHeaders = [{"party":"Counter Party", "rates":"SIMM-Rates and FX", "credit":"SIMM-Credit","equity":"SIMM-Equity","commodity":"SIMM-Commodity","total":"Total SIMM","cpty":"CPTY Details"}];
  
var selectBoxData = [	
					{"number":10},
					{"number":25},
					{"number":50},
					{"number":100}
				];