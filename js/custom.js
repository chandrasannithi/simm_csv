angular.module('myApp.custom',[])
         .controller('customController',['$scope','$rootScope','customeControl',
                function($scope,$rootScope,customeControl){
                $scope.title = "Product View -> Custom SIMM Details";
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                 $scope.customeHeaders = customeControl.custHeader();   
                 $scope.customeCategories =  customeControl.query();
                 $scope.arrLength = $scope.customeCategories.length;
                $scope.entries = customeControl.selectBox(); 
            }])