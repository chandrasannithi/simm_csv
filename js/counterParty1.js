
(function(){
    
    'use strict';
    var counterparty1Controller = function($scope,counter1SVC,$rootScope,SIMMDetails){
        
         $scope.title ="SIMM Details";
        $scope.counter1Header = SIMMDetails.tableHead();
             $scope.entries = SIMMDetails.selectBox();
             if($rootScope.simmulation)
             {
                   $scope.mode = $rootScope.simmulation;
             }
             else
             {
                    $scope.mode = "20160102-Production";   
             }               
                        
        
        counter1SVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                RiskCategory:data[0].trim(),
                                CounterParty: data[1].trim(),
                                Portfolio: data[2].trim(),
                                Currency: data[3].trim(),
                                DeltaMargin: data[4].trim(),
                                VegaMargin: data[5].trim(),
                                CurvatureMargin:data[6].trim(),
                                Total:data[7].trim(),
                             
                            } 
                           
                                
                           
                          
                            
                            
                   // report.data.push(tarr);
                        }
                        lines.push(tarr);
                   // }
                    
                }
                $scope.data = lines;
             // scope.reports.push(lines);
             // $scope.reports.push(report);
            // console.log($scope.data);
            
            
        });
       

        
        
        
    };
    
    
     angular.module('myApp.counter1',[])
    .controller('counterparty1Controller', ['$scope','counter1SVC','$rootScope','SIMMDetails',counterparty1Controller])
    
    
})();