(function(){
    angular.module('myApp.counterDupil')
            .service('counterDupilSVC',['$http','$q',
                function($http,$q){
                    this.getData = function(){
                        var data = $q.defer();
                        $http.get("data/counterDupil.csv").then(function(response){
                            if(response.status == 200)
                            {
                                data.resolve(response);
                            }
                            else
                            {
                                data.reject(response);
                            }
                        });
                        return data.promise;
                    }
                }
            ])
})();


