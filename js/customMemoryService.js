angular.module('myApp.customMS',[])

//Custome Control Starts
            .factory('customeControl',function(){
                return{
                    custHeader: function()
                    {
                        return riskHeaders;
                    },
                    query: function()
                    {
                        return riskCtrlData;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
        
            })
            //Custome Control Ends

  var riskHeaders = [
        {"counter":"Counter Party","risk":"Risk Category","cur":"Currency","delta":"Delta Margin","Vega":"Vega Margin","curvarture":"Curvature Margin","total":"Total" }
    ];

 var riskCtrlData = [
        {"CounterParty":"Citigroup","RiskCategory":"Commodity","Currency":"USD","DeltaMargin":11251118.55,"VegaMargin":1248366.73,"CurvatureMargin":1073751.64,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Rates and  FX","Currency":"EUR","DeltaMargin":500253.11,"VegaMargin":500167.97,"CurvatureMargin":323253.64,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Credit","Currency":"EUR","DeltaMargin":685862.81,"VegaMargin":-685459.48,"CurvatureMargin":512505.69,"Total":323253.64},
        
         {"CounterParty":"JP Morgan","RiskCategory":"Equity","Currency":"USD","DeltaMargin":984007.93,"VegaMargin":982784.74,"CurvatureMargin":808002.27	,"Total":323253.64},
        
         {"CounterParty":"Total:","RiskCategory":"Equity","Currency":"","DeltaMargin":13421242.40,"VegaMargin":2045859.96,"CurvatureMargin":2717513.24,"Total":1293014.56}
        
    ];

  var selectBoxData = [	
					{"number":10},
					{"number":25},
					{"number":50},
					{"number":100}
				];