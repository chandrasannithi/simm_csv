


(function(){
    'use strict';
    var commodityController = function($scope,commoditySVC,$rootScope,commodityTrade){
        $scope.title = "SIMM Views : Commodity Trade Details";
         $scope.entries = commodityTrade.selectBox();
               if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
        
         commoditySVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                CounterParty:data[0],
                                 Poortfolio:data[1],
                                DealID: data[2],
                                ProductType:data[3],
                                SubProduct: data[4],
                                RiskFactorType: data[5],
                                DeltaMargin:data[6],
                               VegaMargin:data[7],
                               CurvatureMargin:data[8],
                               TradeDate:data[9],
                            } 
                          
                            
                            
                   // report.data.push(tarr);
                        }
                        lines.push(tarr);
                   // }
                    
                }
                $scope.data = lines;
             // scope.reports.push(lines);
             // $scope.reports.push(report);
            // console.log($scope.data);
            
            
        });
        
        
        
        
    };
    
    
    
    
    
    angular.module('myApp.commodity',[])
            .controller('commodityController',['$scope','commoditySVC','$rootScope','commodityTrade',commodityController])
})();

 