(function(){
    angular.module('myApp.commodity')
            .service('commoditySVC',['$http','$q',
                function($http,$q){
                    this.getData = function(){
                        var data = $q.defer();
                        $http.get("data/commodity.csv").then(function(response){
                            if(response.status == 200)
                            {
                                data.resolve(response);
                            }
                            else
                            {
                                data.reject(response);
                            }
                        });
                          return data.promise;
                    }
                }                        
                                    
            ])
        
    
})();


