angular.module('myApp.referenceDataMS',[])

.factory('refData',function(){
                return{
                    curvTitle: function()
                    {
                        return curveTblTitle;
                    },
                    curvData: function()
                    {
                        return curveTblData;
                    },
                    country: function()
                    {
                        return countryName;
                    },
                    holidayHeading: function()
                    {
                        return holidayHeading;
                    },
                    holidayList: function()
                    {
                        return holidayContent;
                    },
                    priceHeader: function()
                    {
                        return priceHeaders;
                    },
                    priceContent: function()
                    {
                        return priceContents;
                    },
                    generalTableHeader: function()
                    {
                        return generalTableHeaders;
                    },
                    generalTableContent: function()
                    {
                        return generalTableContents;
                    },
                    girrTbl1: function()
                    {
                        return girrTbl1Content;
                    },
                    girrTbl2: function()
                    {
                        return girrTbl2Content;
                    },
                    girrTbl3: function()
                    {
                        return girrTbl3Content;
                    },
                    girrTbl4: function()
                    {
                        return girrTbl4Content;
                    },
                    credTbl1: function()
                    {
                        return credTbl1Content;
                    },
                    credTbl2: function()
                    {
                        return credTbl2Content;
                    },
                    credTbl3: function()
                    {
                        return credTbl3Content;
                    },
                    credTbl4: function()
                    {
                        return credTbl4Content;
                    },
                    nonCreditTbl1: function()
                    {
                        return nonCreditTbl1Content;
                    },
                    nonCreditTbl2: function()
                    {
                        return nonCreditTbl2Content;
                    },
                    nonCreditTbl3: function()
                    {
                        return nonCreditTbl3Content;
                    },
                    nonCreditTbl4: function()
                    {
                        return credTbl3Content;
                    },
                    comodity1: function()
                    {
                        return comodity1Content;
                    },
                    comodity2: function()
                    {
                        return comodity2Content;
                    },
                    comodity3: function()
                    {
                        return comodity3Content;
                    },
                    comodity4: function()
                    {
                        return comodity4Content;
                    },
                    comodity5: function()
                    {
                        return comodity5Content;
                    },
                    fxs: function()
                    {
                        return fxsContent;
                    }
                }
                
            });


//Reference Data
    
    var curveTblTitle = [
        {"curveName":"Curve Name", "curveType":"Curve Type","curve":"Curve","tenor":"Tenor","discountFactor":"Discount Factor"}
    ];
    
    var curveTblData = [
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"0D", "discountFactor":0.001571524},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1M", "discountFactor":0.000934099},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2M", "discountFactor":0.000948444},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3M", "discountFactor":0.000935866},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6M", "discountFactor":0.001027672},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9M", "discountFactor":0.001280398},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1Y", "discountFactor":0.001841773},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2Y", "discountFactor":0.005475661},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3Y", "discountFactor":0.009655095},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"4Y", "discountFactor":0.013191876},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"5Y", "discountFactor":0.015783922},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6Y", "discountFactor":0.017845475},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"7Y", "discountFactor":0.019539006},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"8Y", "discountFactor":0.02095814},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9Y", "discountFactor":0.02217629},        
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"10Y", "discountFactor":0.023239034},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"12Y", "discountFactor":0.02501226},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"15Y", "discountFactor":0.026896017},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"20Y", "discountFactor":0.028543824},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"25Y", "discountFactor":0.029327868},
        {"curveName":"LIBOR-3M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"30Y", "discountFactor":0.029693673},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"0D", "discountFactor":0.011571524},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1M", "discountFactor":0.010934099},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2M", "discountFactor":0.010948444},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3M", "discountFactor":0.010935866},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6M", "discountFactor":0.011027672},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9M", "discountFactor":0.011280398},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"1Y", "discountFactor":0.011841773},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"2Y", "discountFactor":0.015475661},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"3Y", "discountFactor":0.019655095},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"4Y", "discountFactor":0.023191876},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"5Y", "discountFactor":0.025783922},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"6Y", "discountFactor":0.027845475},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"7Y", "discountFactor":0.029539006},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"8Y", "discountFactor":0.03095814},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"9Y", "discountFactor":0.03217629},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"10Y", "discountFactor":0.033239034},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"12Y", "discountFactor":0.03501226},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"15Y", "discountFactor":0.036896017},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"20Y", "discountFactor":0.038543824},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"25Y", "discountFactor":0.039327868},
        {"curveName":"LIBOR-6M", "curveType":"Discount Factor", "curve":"USD-Disc", "tenor":"30Y", "discountFactor":0.039693673}
        
        
    ];
    
    
    
    var holidayHeading = [
        {"holidayDate":"Holiday Date", "description":"Holiday Description"}
    ];
    
    var holidayContent= [
         {"holidayDate":'2016-01-01', "description":"New Year Day"},
        {"holidayDate":'2016-01-18', "description":"Martin Luther King Jr.Day"},
        {"holidayDate":'2016-02-15', "description":"Presidents Day(Washingtons Birthday)"},
        {"holidayDate":'2016-05-30', "description":"Memorial Day"},
        {"holidayDate":'2016-07-04', "description":"Independance Day"},
        {"holidayDate":'2016-09-05', "description":"Labor Day"},
        {"holidayDate":'2016-10-10', "description":"Columbus Day"},
        {"holidayDate":'2016-11-11', "description":"Veterans Day"},
        {"holidayDate":'2016-11-24', "description":"Thanks giving Day"},
        {"holidayDate":'2016-12-25', "description":"Christmas Day"},
    ];
    
    var countryName = [
        {"name":"Select Country"},
        {"name":"USA"},
    ];
   
    var priceHeaders = [
    
        {"date":"Date","security":"Security","price":"Price"}
    ];
    
    var priceContents = [
        {"date":'04/10/2016', "security":"LIBOR 3M", "price":0.225},
         {"date":'04/10/2016', "security":"LIBOR 6M", "price":0.325},
         {"date":'04/10/2016', "security":"LIBOR 9M", "price":0.425},
         {"date":'04/10/2016', "security":"LIBOR 12M", "price":0.525},
         {"date":'04/10/2016', "security":"EDU6", "price":0.525},
         {"date":'04/10/2016', "security":"EDM6", "price":0.525}
    ];
    
    var generalTableHeaders = [
        
        {"riskClass":"Risk Class", "girr":"GIRR", "credit":"Credit (Qualifying)", "nonCredit":"Credit (Non-Qualifying)", "equity":"Equity", "commodity":"Commodity","fx":"FX"}
    ];
    
    var generalTableContents = [
        {"riskClass":"GIRR", "girr":1, "credit":"9%", "nonCredit":"10%", "equity":"18%", "commodity":"32%", "fx":"27%"},
        {"riskClass":"Credit (Qualifying)", "girr":"9%", "credit":1, "nonCredit":"24%", "equity":"58%", "commodity":"34%", "fx":"29%"},
        {"riskClass":"Credit (Non-Qualifying)", "girr":"10%", "credit":"24%", "nonCredit":1, "equity":"23%", "commodity":"24%", "fx":"12%"},
        {"riskClass":"Equity", "girr":"18%", "credit":"58%", "nonCredit":"23%", "equity":1, "commodity":"26%", "fx":"31%"},
        {"riskClass":"Commodity", "girr":"32%", "credit":"34%", "nonCredit":"24%", "equity":"26%", "commodity":1, "fx":"37%"},
        {"riskClass":"FX", "girr":"27%", "credit":"29%", "nonCredit":"12%", "equity":"31%", "commodity":"37%", "fx":1}
    ];
    
    var girrTbl1Content= [
        {"currencyGrp":"All Other Currencies", "concentration":"TBD"},
        {"currencyGrp":"G10+DKK", "concentration":"TBD"}
    ];
    
    var girrTbl2Content = [
        {"currency":"AUD", "volatility":"Regular Volatility"},
        {"currency":"CAD", "volatility":"Regular Volatility"},
        {"currency":"CHF", "volatility":"Regular Volatility"},
        {"currency":"DKK", "volatility":"Regular Volatility"},
        {"currency":"EUR", "volatility":"Regular Volatility"},
        {"currency":"GBP", "volatility":"Regular Volatility"},
        {"currency":"HKD", "volatility":"Regular Volatility"},
        {"currency":"JPY", "volatility":"Regular Volatility"},
        {"currency":"KRW", "volatility":"Regular Volatility"},
        {"currency":"NOK", "volatility":"Regular Volatility"},
        {"currency":"NZD", "volatility":"Regular Volatility"},
        {"currency":"SEK", "volatility":"Regular Volatility"},
        {"currency":"SGD", "volatility":"Regular Volatility"},
        {"currency":"TWD", "volatility":"Regular Volatility"},
        {"currency":"USD", "volatility":"Regular Volatility"},
    ];
    
    var girrTbl3Content =[
        {"bucket":"High Volatility Currencies", "one":89, "two":89, "three":89, "four":94, "five":104, "six":99, "seven":96, "eight":99, "nine":87, "ten":97, "eleven":97, "twele": 98},
        {"bucket":"Low Volatility Currencies", "one":10, "two":10, "three":10, "four":10, "five":13, "six":16, "seven":18, "eight":20, "nine":25, "ten":22, "eleven":22, "twele":23 },
        {"bucket":"Regular Currencies", "one":77, "two":77, "three":77, "four":64, "five":58, "six":49, "seven":47, "eight":47, "nine":45, "ten":45, "eleven":48, "twele":56 },
        {"bucket":"Tenor", "one":"2w", "two":"1m", "three":"3m", "four":"6m", "five":"1y", "six":"2y", "seven":"3y", "eight":"5y", "nine":"10y", "ten":"15y", "eleven":"20y", "twele":"30y" },
        
    ];
    
    var girrTbl4Content = [
        {"data":"k-10 (30yr)", "tm":0.129, "sm":0.296, "oy":0.471, "ty":0.602, "thy":0.69, "fy":0.812, "teny":0.931, "fify":0.97, "twey":0.988, "thiry":1},
         {"data":"k=1 (3m)", "tm":1, "sm":0.782, "oy":0.618, "ty":0.498, "thy":0.438, "fy":0.361, "teny":0.27, "fify":0.196, "twey":0.174, "thiry":0.129},
         {"data":"k=2 (6m)", "tm":0.782, "sm":1, "oy":0.618, "ty":0.739, "thy":0.438, "fy":0.569, "teny":0.444, "fify":0.375, "twey":0.349, "thiry":0.296},
         {"data":"k=3 (1yr)", "tm":0.618, "sm":0.84, "oy":1, "ty":0.917, "thy":0.859, "fy":0.757, "teny":0.626, "fify":0.555, "twey":0.526, "thiry":0.471},
         {"data":"k=4 (2yr)", "tm":0.498, "sm":0.739, "oy":0.917, "ty":1, "thy":0.976, "fy":0.895, "teny":0.749, "fify":0.69, "twey":0.66, "thiry":0.602},
         {"data":"k=5 (3yr)", "tm":0.438, "sm":0.667, "oy":0.859, "ty":0.976, "thy":1, "fy":0.958, "teny":0.831, "fify":0.779, "twey":0.746, "thiry":0.69},
         {"data":"k=6 (5yr)", "tm":0.361, "sm":0.569, "oy":0.757, "ty":0.895, "thy":0.958, "fy":1, "teny":0.925, "fify":0.893, "twey":0.859, "thiry":0.812},
         {"data":"k=7 (10yr)", "tm":0.27, "sm":0.444, "oy":0.626, "ty":0.749, "thy":0.831, "fy":0.925, "teny":1, "fify":0.98, "twey":0.961, "thiry":0.931},
         {"data":"k=8 (15yr)", "tm":0.196, "sm":0.375, "oy":0.555, "ty":0.69, "thy":0.779, "fy":0.893, "teny":0.98, "fify":1, "twey":0.989, "thiry":0.97},
         {"data":"k=9 (20yr)", "tm":0.174, "sm":0.349, "oy":0.526, "ty":0.66, "thy":0.746, "fy":0.859, "teny":0.961, "fify":0.989, "twey":1, "thiry":0.988}
    ];
    
    //Credit Qualification
    
    var credTbl1Content = [
        {"bucketNo":1, "crQuality":"Investment grade (IG)", "sector":"Sovereigns including central banks", "riskWeight":97},
        {"bucketNo":2, "crQuality":"Investment grade (IG)", "sector":"Financials including government-backed financials", "riskWeight":110},
        {"bucketNo":3, "crQuality":"Investment grade (IG)", "sector":"Basic materials energy industrials", "riskWeight":73},
        {"bucketNo":4, "crQuality":"Investment grade (IG)", "sector":"Consumer", "riskWeight":65},
        {"bucketNo":5, "crQuality":"Investment grade (IG)", "sector":"Technology telecommunications", "riskWeight":52},
        {"bucketNo":6, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Health care utilities local government government-backed corporates (non- financial)", "riskWeight":39},
        {"bucketNo":7, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Sovereigns including central banks", "riskWeight":198},
        {"bucketNo":8, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Financials including government backed financials", "riskWeight":638},
        {"bucketNo":9, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Basic materials energy industrials", "riskWeight":210},
        {"bucketNo":10, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Consumer", "riskWeight":375},
        {"bucketNo":11, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"Technology telecommunications", "riskWeight":240},
        {"bucketNo":12, "crQuality":"High yield (HY) & non-rated (NR)", "sector":"	Health care utilities local government government-backed corporates (non- financial)", "riskWeight":152},
        {"bucketNo":"Residual", "crQuality":"", "sector":"", "riskWeight":638},
    ];
    
    var credTbl2Content = [
        {"data":"Aggregated Sensitivities", "bucket":"98%", "riskWeight":"55%" },
         {"data":"Residual Bucket", "bucket":"50%", "riskWeight":"50%" }
    ];
    
    var credTbl3Content = [
        {"crRisk":"Qualifying", "concentration":"TBD" },
        {"crRisk":"Non-Qualifying", "concentration":"TBD" }
    ];
    
    var credTbl4Content = [
        {"bucket":"b=1", "c1":1, "c2":0.51, "c3":0.47, "c4":0.49, "c5":0.46, "c6":0.47, "c7":0.41, "c8":0.36, "c9":0.45, "c10":0.47, "c11":0.47, "c12": 0.43},
        {"bucket":"b=2", "c1":0.51, "c2":1, "c3":0.52, "c4":0.52, "c5":0.49, "c6":0.52, "c7":0.37, "c8":0.41, "c9":0.51, "c10":0.5, "c11":0.51, "c12": 0.46},
        {"bucket":"b=3", "c1":0.47, "c2":0.52, "c3":1, "c4":0.54, "c5":0.51, "c6":0.55, "c7":0.37, "c8":0.37, "c9":0.51, "c10":0.49, "c11":0.5, "c12":0.47 },
        {"bucket":"b=4", "c1":0.49, "c2":0.52, "c3":0.54, "c4":1, "c5":0.53, "c6":0.56, "c7":0.36, "c8":0.37, "c9":0.52, "c10":0.51, "c11":0.51, "c12":0.46 },
        {"bucket":"b=5", "c1":0.46, "c2":0.49, "c3":0.51, "c4":0.53, "c5":1, "c6":0.54, "c7":0.35, "c8":0.35, "c9":0.49, "c10":0.48, "c11":0.5, "c12": 0.44},
        {"bucket":"b=6", "c1":0.47, "c2":0.52, "c3":0.55, "c4":0.56, "c5":0.54, "c6":1, "c7":0.37, "c8":0.37, "c9":0.52, "c10":0.49, "c11":0.51, "c12": 0.48},
        {"bucket":"b=7", "c1":0.41, "c2":0.37, "c3":0.37, "c4":0.36, "c5":0.35, "c6":0.37, "c7":1, "c8":0.29, "c9":0.36, "c10":0.52, "c11":0.36, "c12":0.36 },
        {"bucket":"b=8", "c1":0.36, "c2":0.41, "c3":37, "c4":0.37, "c5":0.35, "c6":0.37, "c7":0.29, "c8":1, "c9":0.37, "c10":0.36, "c11":0.37, "c12": 0.33},
        {"bucket":"b=9", "c1":0.45, "c2":0.51, "c3":0.51, "c4":0.52, "c5":0.49, "c6":0.52, "c3":0.36, "c8":0.37, "c9":1, "c10":0.49, "c11":0.5, "c12":0.46 },
        {"bucket":"b=10", "c1":0.47, "c2":0.5, "c3":0.49, "c4":0.51, "c5":0.48, "c6":0.49, "c7":0.34, "c8":0.36, "c9":0.49, "c10":1, "c11":0.49, "c12": 0.46},
        {"bucket":"b=11", "c1":0.47, "c2":0.51, "c3":0.5, "c4":0.51, "c5":0.5, "c6":0.51, "c7":0.36, "c8":0.37, "c9":0.5, "c10":0.49, "c11":1, "c12": 0.46},
        {"bucket":"b=12", "c1":0.43, "c2":0.46, "c3":0.47, "c4":0.46, "c5":0.44, "c6":0.48, "c7":0.36, "c8":0.33, "c9":0.46, "c10":0.46, "c11":0.46, "c12":1 }
    ];
   
    var nonCreditTbl1Content = [
        {"bktNo":1, "creditQlt":"Investment Grade (IG)", "sector":"RMBS/CMBS"},
         {"bktNo":2, "creditQlt":"High	Yield(HY) and Not Rated(NR)", "sector":"RMBS/CMBS"},
         {"bktNo":"Residual", "creditQlt":"", "sector":""}
    ];
    
    var nonCreditTbl2Content = [
        {"data":"regated Sensitivities", "bucket":"60%", "riskWeight":"21%"},
        {"data":"Residual Bucket", "bucket":"50%", "riskWeight":"50%"}
    ];
    
    var nonCreditTbl3Content = [
        {"bucket":1, "riskWeight":169},
        {"bucket":2, "riskWeight":1646},
         {"bucket":"Residual", "riskWeight":1646}
    ];
    
    //Commodity
    var comodity1Content = [
        {"bktNo":1, "commodity":"Coal", "riskWeight":9, "riskCorr":"71%", "concentration": "TBA A"},
        {"bktNo":2, "commodity":"Crude", "riskWeight":19, "riskCorr":"92%", "concentration":"TBA A" },
        {"bktNo":3, "commodity":"Light Ends", "riskWeight":18, "riskCorr":"97%", "concentration":"TBA A" },
        {"bktNo":4, "commodity":"Middle Distillates", "riskWeight":13, "riskCorr":"97%", "concentration":"TBA A" },
        {"bktNo":5, "commodity":"Heavy Distillates", "riskWeight":24, "riskCorr":"99%", "concentration":"TBA A" },
        {"bktNo":6, "commodity":"North America Natural Gas", "riskWeight":17, "riskCorr":"98%", "concentration":"TBA A" },
        {"bktNo":7, "commodity":"European Natural Gas", "riskWeight":21, "riskCorr":"100%", "concentration": "TBA A"},
        {"bktNo":8, "commodity":"North American Power", "riskWeight":35, "riskCorr":"69%", "concentration": "TBA B"},
        {"bktNo":9, "commodity":"European Power", "riskWeight":20, "riskCorr":"47%", "concentration":"TBA B" },
        {"bktNo":10, "commodity":"Freight", "riskWeight":50, "riskCorr":"1%", "concentration": "TBA B"},
        {"bktNo":11, "commodity":"Base Metals", "riskWeight":21, "riskCorr":"67%", "concentration": "TBA C"},
        {"bktNo":12, "commodity":"Precious Metals", "riskWeight":19, "riskCorr":"70%", "concentration": "TBA C"},
        {"bktNo":13, "commodity":"Grains", "riskWeight":17, "riskCorr":"68%", "concentration": "TBA C"},
        {"bktNo":14, "commodity":"Softs", "riskWeight":15, "riskCorr":"22%", "concentration": "TBA C"},
        {"bktNo":15, "commodity":"Livestock", "riskWeight":8, "riskCorr":"50%", "concentration": "TBA C"},
        {"bktNo":16, "commodity":"Other", "riskWeight":50, "riskCorr":"0%", "concentration": "TBA C"}
    ];
    
    var comodity2Content = [
        {"bucket":"b=1", "c1":"1%", "c2":"11%", "c3":"16%", "c4":"13%", "c5":"10%", "c6":"6%", "c7":"20%", "c8":"5%", "c9":"17%", "c10":"3%", "c11":"18%", "c12":"9%", "c13":"10%", "c14":"5%", "c15":"4%", "c16":"0%" },
        
         {"bucket":"b=10", "c1":"3%", "c2":"14%", "c3":"12%", "c4":"8%", "c5":"21%", "c6":"15%", "c7":"20%", "c8":"12%", "c9":"12%", "c10":"1%", "c11":"10", "c12":"7%", "c13":"9%", "c14":"10%", "c15":"16%", "c16":"0%" },
        
        {"bucket":"b=11", "c1":"10%", "c2":"30%", "c3":"32%", "c4":"31%", "c5":"34%", "c6":"0%", "c7":"6%", "c8":"-1%", "c9":"10%", "c10":"10%", "c11":"1%", "c12":"46%", "c13":"20%", "c14":"26%", "c15":"18%", "c16":"0%" },
        
        {"bucket":"b=12", "c1":"9%", "c2":"31%", "c3":"26%", "c4":"25%", "c5":"32%", "c6":"0%", "c7":"6%", "c8":"0%", "c9":"6%", "c10":"7%", "c11":"46%", "c12":"1%", "c13":"25%", "c14":"23%", "c15":"14%", "c16":"0%" },
        
        {"bucket":"b=13", "c1":"10%", "c2":"26%", "c3":"16%", "c4":"15%", "c5":"27%", "c6":"23%", "c7":"12%", "c8":"18%", "c9":"12%", "c10":"9%", "c11":"20%", "c12":"25%", "c13":"1%", "c14":"29%", "c15":"6%", "c16":"0%" },
        
        {"bucket":"b=14", "c1":"5%", "c2":"26%", "c3":"12%", "c4":"20%", "c5":"29%", "c6":"15%", "c7":"9%", "c8":"11%", "c9":"10%", "c10":"10%", "c11":"26%", "c12":"23%", "c13":"29%", "c14":"1%", "c15":"15%", "c16":"0%" },
        
        {"bucket":"b=15", "c1":"4%", "c2":"12%", "c3":"22%", "c4":"9%", "c5":"12%", "c6":"7%", "c7":"9%", "c8":"4%", "c9":"10%", "c10":"16%", "c11":"18%", "c12":"14%", "c13":"6%", "c14":"15%", "c15":"1%", "c16":"0%" },
        
        {"bucket":"b=16", "c1":"0%", "c2":"0%", "c3":"0%", "c4":"0%", "c5":"0%", "c6":"0%", "c7":"0%", "c8":"0%", "c9":"0%", "c10":"0%", "c11":"0%", "c12":"0%", "c13":"0%", "c14":"0%", "c15":"0%", "c16":"1%" },
        
        {"bucket":"b=2", "c1":"11%", "c2":"1%", "c3":"95%", "c4":"95%", "c5":"93%", "c6":"15%", "c7":"27%", "c8":"19%", "c9":"20%", "c10":"14%", "c11":"30%", "c12":"31%", "c13":"26%", "c14":"26%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=3", "c1":"16%", "c2":"95%", "c3":"1%", "c4":"92%", "c5":"90%", "c6":"17%", "c7":"24%", "c8":"14%", "c9":"17%", "c10":"12%", "c11":"32%", "c12":"26%", "c13":"16%", "c14":"22%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=4", "c1":"13%", "c2":"95%", "c3":"92%", "c4":"1%", "c5":"90%", "c6":"18%", "c7":"26%", "c8":"8%", "c9":"17%", "c10":"8%", "c11":"31%", "c12":"25%", "c13":"15%", "c14":"20%", "c15":"9%", "c16":"0%" },
        
        {"bucket":"b=5", "c1":"10%", "c2":"93%", "c3":"90%", "c4":"90%", "c5":"1%", "c6":"18%", "c7":"37%", "c8":"13%", "c9":"30%", "c10":"21%", "c11":"34%", "c12":"32%", "c13":"27%", "c14":"29%", "c15":"12%", "c16":"0%" },
        
        {"bucket":"b=6", "c1":"6%", "c2":"15%", "c3":"17%", "c4":"80%", "c5":"18%", "c6":"1%", "c7":"7%", "c8":"62%", "c9":"3%", "c10":"15%", "c11":"0%", "c12":"0%", "c13":"23%", "c14":"15%", "c15":"7%", "c16": "0%"},
        
        {"bucket":"b=7", "c1":"20%", "c2":"27%", "c3":"24%", "c4":"26%", "c5":"37%", "c6":"7%", "c7":"1%", "c8":"7%", "c9":"66%", "c10":"20%", "c11":"6%", "c12":"6%", "c13":"12%", "c14":"9%", "c15":"9%", "c16":"0%" },
        
        {"bucket":"b=8", "c1":"5%", "c2":"19%", "c3":"14%", "c4":"90%", "c5":"13%", "c6":"62%", "c7":"7%", "c8":"1%", "c9":"9%", "c10":"12%", "c11":"97%", "c12":"0%", "c13":"18%", "c14":"11%", "c15":"4%", "c16":"0%" },
        
        {"bucket":"b=9", "c1":"17%", "c2":"20%", "c3":"17%", "c4":"8%", "c5":"30%", "c6":"3%", "c7":"66%", "c8":"9%", "c9":"1%", "c10":"12%", "c11":"10%", "c12":"6%", "c13":"12%", "c14":"10%", "c15":"10%", "c16":"0%" }
        
       
    ];
    
    var comodity3Content = [
        {"bktNo":1, "size":"Large", "region":"Emerging markets", "riskWeight":22, "riskCorre":"14%", "sector":'"Consumer goods and services transportation and storage administrative and support service activities utilities"'},
        {"bktNo":2, "size":"Large", "region":"Emerging markets", "riskWeight":28, "riskCorre":"24%", "sector":'"Telecommunications industrials"'},        
        {"bktNo":3, "size":"Large", "region":"Emerging markets", "riskWeight":28, "riskCorre":"25%", "sector":"Basic materials energy agriculture manufacturing mining and quarrying"},
        {"bktNo":4, "size":"Large", "region":"Emerging markets", "riskWeight":25, "riskCorre":"20%", "sector":'	"Financials including gov’t-backed financials real estate activities technology"'},
        {"bktNo":5, "size":"Large", "region":"Developed markets", "riskWeight":18, "riskCorre":"26%", "sector":"Consumer goods and services transportation and storage administrative and support service activities utilities"},
        {"bktNo":6, "size":"Large", "region":"Developed markets", "riskWeight":20, "riskCorre":"34%", "sector":"Telecommunications industrials"},
        {"bktNo":7, "size":"Large", "region":"Developed markets", "riskWeight":24, "riskCorre":"33%", "sector":"Basic materials energy agriculture manufacturing mining and quarrying"},
        {"bktNo":8, "size":"Large", "region":"Developed markets", "riskWeight":23, "riskCorre":"34%", "sector":"Financials including gov’t-backed financials real estate activities technology"},
        {"bktNo":9, "size":"Small", "region":"Emerging markets", "riskWeight":26, "riskCorre":"21%", "sector":"All sectors"},
        {"bktNo":10, "size":"Small", "region":"Developed markets", "riskWeight":27, "riskCorre":"24%", "sector":"All sectors"},
        {"bktNo":11, "size":"All", "region":"All", "riskWeight":15, "riskCorre":"63%", "sector":"Indexes Funds ETFs"},
        {"bktNo":12, "size":"residual", "region":"Developed markets", "riskWeight":28, "riskCorre":"0%", "sector":0}
        
    ];
    
    var comodity4Content = [
        {"equityRisk":"Developed Markets", "concentration":"TBD"},
        {"equityRisk":"Emerging Markets", "concentration":"TBD"},
        {"equityRisk":"Indexes Funds ETFs", "concentration":"TBD"}
        
    ];
    
    var comodity5Content = [
        {"bucket":"b=1", "c1":1, "c2":0.17, "c3":0.18, "c4":0.18, "c5":0.8, "c6":0.1, "c7":0.1, "c8":0.11, "c9":0.16, "c10":0.8, "c11":0.18},
         {"bucket":"b=2", "c1":0.17, "c2":1, "c3":0.24, "c4":0.19, "c5":0.7, "c6":0.1, "c7":0.9, "c8":0.1, "c9":0.19, "c10":0.7, "c11":0.18},
        {"bucket":"b=3", "c1":0.18, "c2":0.24, "c3":1, "c4":0.21, "c5":0.9, "c6":0.12, "c7":0.13, "c8":0.13, "c9":0.2, "c10":0.1, "c11":0.24},
        {"bucket":"b=4", "c1":0.16, "c2":0.19, "c3":0.21, "c4":1, "c5":0.13, "c6":0.17, "c7":0.16, "c8":0.17, "c9":0.2, "c10":0.13, "c11":0.3},
        {"bucket":"b=5", "c1":0.8, "c2":0.7, "c3":0.9, "c4":0.13, "c5":1, "c6":0.28, "c7":0.24, "c8":0.28, "c9":0.1, "c10":0.23, "c11":0.38},
        {"bucket":"b=6", "c1":0.1, "c2":0.1, "c3":0.12, "c4":0.17, "c5":0.28, "c6":1, "c7":0.3, "c8":0.33, "c9":0.13, "c10":0.26, "c11":0.45},
        {"bucket":"b=7", "c1":0.1, "c2":0.9, "c3":0.13, "c4":0.16, "c5":0.24, "c6":0.3, "c7":1, "c8":0.29, "c9":0.13, "c10":0.25, "c11":0.42},
        {"bucket":"b=8", "c1":0.11, "c2":0.1, "c3":0.13, "c4":0.17, "c5":0.28, "c6":0.33, "c7":0.29, "c8":1, "c9":0.14, "c10":0.27, "c11":0.45},
        {"bucket":"b=9", "c1":0.16, "c2":0.19, "c3":0.2, "c4":0.2, "c5":0.1, "c6":0.13, "c7":0.13, "c8":0.14, "c9":1, "c10":0.11, "c11":0.25},
        {"bucket":"b=10", "c1":0.8, "c2":0.7, "c3":0.1, "c4":0.13, "c5":0.23, "c6":0.26, "c7":0.25, "c8":0.27, "c9":0.11, "c10":1, "c11":0.34},
        {"bucket":"b=11", "c1":0.18, "c2":0.18, "c3":0.24, "c4":0.3, "c5":0.38, "c6":0.45, "c7":0.42, "c8":0.45, "c9":0.25, "c10":0.34, "c11":1},
        
    ];
    
    var fxsContent = [
        {"fxRisk":"All Currencies", "concentration":"TBD"}
    ];
    
    var selectBoxData = [	
					{"number":10},
					{"number":25},
					{"number":50},
					{"number":100}
				];