angular.module('myApp.simmActions',[])
         .controller('SIMM_ActionsController',['$scope','$rootScope','simmView',
                function($scope,$rootScope,simmView){
                    $scope.title = "SIMM Actions: CRIF Output";
                     if($rootScope.simmulation)
                        {
                           $scope.mode = $rootScope.simmulation;
                        }
                        else
                        {
                            $scope.mode = "20160102-Production";   
                        }
                    $scope.simmHeaders = simmView.custHeader();
                    $scope.simmContents = simmView.query();
                    $scope.arrLength = $scope.simmContents.length;
                    $scope.blazerTitle = "SIMM Breaches Overview COB April 10, 2016";
                    $scope.blazerHeaders = simmView.blazerHeader();
                    $scope.blazerContents = simmView.queryBlazer();
                    $scope.blazeerLength = $scope.blazerContents.length;

                }])