angular.module('myApp.simmulation',[])
.controller('simmulationController',['$scope','simmulationData', '$rootScope',function($scope,simmulationData,$rootScope){
                $scope.title="Customize Inputs – Market Data : Simmulation";
                
                var simmulation = true;
                
                if($rootScope.simmulation)
                {
                    $scope.txtField = $rootScope.simmulation;
                    $scope.radioCheck = $rootScope.simmulation;
                    
                }
                else
                {
                   $scope.txtField = "20160102-Production";
                   $scope.radioCheck = "20160102-Production";
                   $rootScope.simmulation = "20160102-Production";
                }
               
                
                if(simmulation === true)
                {
                    $scope.simms = simmulationData.query();
                    $scope.prodLength = $scope.simms.length;
                }
                $scope.fun = function(data)
                {
                   $scope.txtField = data;
				   $rootScope.simmulation = $scope.txtField;                   
                    
                }
                $scope.lordPortfolios = simmulationData.lordPortfolios();
                $scope.lordCountries = simmulationData.lordCountries();
                $scope.tradeHeaders = simmulationData.loadHeaders();
                $scope.tradeContents = simmulationData.tradeContent();
                
            }])