(function(){
    angular.module('myApp.dashboard')
        .service('dashboardSVC',['$http','$q',
            function($http,$q){
                this.getData = function(){
                    var data = $q.defer();
                    $http.get("data/dashboard.csv").then(function(response){
                        if(response.status == 200)
                            {
                                data.resolve(response);
                            }
                        else{
                            data.reject(response);
                        }
                        
                    });
                    return data.promise;
                }
            }                        
                                
                                
        ])
            
})();