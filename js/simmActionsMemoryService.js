angular.module('myApp.simmActionsMS',[])

    //SIMM View starts
                
            .factory('simmView',function(){
                return{
                    custHeader: function()
                    {
                        return simmActionHeader;
                    },
                    query: function()
                    {
                        return simmActionContent;
                    },
                    blazerHeader: function()
                    {
                        return blazerHeaders;
                    },
                    queryBlazer: function()
                    {
                        return blazerContent;
                    }
                }
            })
            //SIMM View Ends

  var simmActionHeader = [
        {"risk":"Risk  Type",  "qualifier":"Qualifier", "bucket":"Bucket", "label1":"Label1", "label2":"Label2", "amount":"Amount", "amountCurrency":"AmountCurrency", "amountUSD":"AmountUSD", "tradelD":"TradelD", "portfolioID":"PortfolioID"}
    ];
    

 //Simm Action Table Content
    var simmActionContent =[
        {"riskType":"Risk_Commodity", "qualifier":"Precious Metals Gold", "bucket":"12 (Precious Metals)", "label1":"", "label2":"", "amount":66124, "amountCurrency":"USD", "amountUSD":66124, "tradelD":121, "portfolioID":"Commodity_5455" },
        
        {"riskType":"Risk_CommodityVol", "qualifier":"Precious Metals Gold", "bucket":"12 (Precious Metals)", "label1":"3m", "label2":"", "amount":23754, "amountCurrency":"USD", "amountUSD":23754, "tradelD":343, "portfolioID":"Commodity_6767" },
        
        {"riskType":"Risk_CreditQ", "qualifier":"ISIN: XS1081333921", "bucket":"3 (Investment grade)", "label1":"5Y", "label2":"", "amount":4939, "amountCurrency":"USD", "amountUSD":4939, "tradelD":10, "portfolioID":"Credit_5678" },
        
        {"riskType":"Risk_Equity", "qualifier":"FTSE100", "bucket":"11 (Indexes,Funds,ETFs)", "label1":"", "label2":"", "amount":84498, "amountCurrency":"USD", "amountUSD":84498, "tradelD":110, "portfolioID":"Equity_4354" },
        
        {"riskType":"Risk_EquityVol", "qualifier":"FTSE100", "bucket":"11 (Indexes,Funds,ETFs)", "label1":"1y", "label2":"", "amount":59578, "amountCurrency":"USD", "amountUSD":59578, "tradelD":122, "portfolioID":"Equity_4354" },
        
        {"riskType":"Risk_FX", "qualifier":"EUR", "bucket":"", "label1":"", "label2":"", "amount":-92320, "amountCurrency":"USD", "amountUSD":-92320, "tradelD":122, "portfolioID":"RatesFX_4532" },
        
        {"riskType":"Risk_FX", "qualifier":"USD", "bucket":"", "label1":"", "label2":"", "amount":99765, "amountCurrency":"USD", "amountUSD":99765, "tradelD":1344, "portfolioID":"RatesFX_4532" },
        
        {"riskType":"Risk_FXVol", "qualifier":"USDJPY", "bucket":"", "label1":"3m", "label2":"", "amount":19768, "amountCurrency":"USD", "amountUSD":19768, "tradelD":2120, "portfolioID":"RatesFX_7373"},
        
        {"riskType":"Risk_Inflation", "qualifier":"USD", "bucket":"", "label1":"", "label2":"", "amount":-6968, "amountCurrency":"USD", "amountUSD":-6968, "tradelD":223, "portfolioID":"RatesFX_6676" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"3m", "label2":"Libor3m", "amount":252.6303142, "amountCurrency":"USD", "amountUSD":252.6303142, "tradelD":1, "portfolioID":"IR_1234"},
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"6m", "label2":"Libor3m", "amount":255.2378803, "amountCurrency":"USD", "amountUSD":255.2378803, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"1Y", "label2":"Libor3m", "amount":504.1483298, "amountCurrency":"USD", "amountUSD":504.1483298, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"2Y", "label2":"Libor3m", "amount":1006.325006, "amountCurrency":"USD", "amountUSD":1006.325006, "tradelD":1, "portfolioID":"IR_1234"},
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"3Y", "label2":"Libor3m", "amount":996.9373229	, "amountCurrency":"USD", "amountUSD":996.9373229, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"5Y", "label2":"Libor3m", "amount":1718.802164, "amountCurrency":"USD", "amountUSD":1718.802164, "tradelD":1, "portfolioID":"IR_1234" },
        
        {"riskType":"Risk_IRCurve", "qualifier":"USD", "bucket":"1 (Regular_Volatility_Currency)", "label1":"5y", "label2":"", "amount":-4881, "amountCurrency":"USD", "amountUSD":-4881, "tradelD":55, "portfolioID":"RatesFX_6676"},
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"1y", "label2":"", "amount":1640, "amountCurrency":"USD", "amountUSD":1640, "tradelD":3434, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"2y", "label2":"", "amount":97420, "amountCurrency":"USD", "amountUSD":97420, "tradelD":3435, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"3y", "label2":"", "amount":108502, "amountCurrency":"USD", "amountUSD":108502, "tradelD":3436, "portfolioID":"RatesFX_3422"},
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"5y", "label2":"", "amount":185665, "amountCurrency":"USD", "amountUSD":185665, "tradelD":3437, "portfolioID":"RatesFX_3422" },
        
        {"riskType":"Risk_IRVol", "qualifier":"USD", "bucket":"", "label1":"10y", "label2":"", "amount":76930, "amountCurrency":"USD", "amountUSD":76930, "tradelD":3438, "portfolioID":"RatesFX_3422" }
        
    ];


 //Blazers Header
    var blazerHeaders = [
        {"counterParty":"CounterParty","portfolio":"Portfolio","breach":"Breach"}
    ];
    
    //Blazers Content
    var blazerContent = [
        {"counterParty":"Barclays PLC","portfolio":"TIPS Portfolio","breach":"+1.1%"},
        {"counterParty":"Citigroup","portfolio":"FX EM Portfolio","breach":"+2%"},
        {"counterParty":"JP Morgan & Chase","portfolio":"Credit Trading","breach":"+3%"},
        {"counterParty":"JP Morgan & Chase","portfolio":"IRS Euro Area","breach":"+5%"}
    ];