angular.module('myApp.counter2MS',[])
 // Counter Party 2 Module starts	
            .factory('riskCategory',function(){
                return{
                    
                   
                    tableHead: function()
                    {
                        return counter2Headers;
                    },
                    selectBox: function()
                    {
                        return selectBoxData;
                    }
                }
            
            }) // Counter Party 2 Module Ends


 
    
    // Counter2 table heading
    var counter2Headers = [
        {"risk":"Risk Category", "counter": "Counter Party", "csaID":"CSA ID","trading":"Trading Desk","currency":"Currency","delta":"Delta Margin","vega":"Vega Margin","curvature":"Curvature Margin","total":"Total","trades":"Trades"}
    ];

  var selectBoxData = [	
					{"number":10},
					{"number":25},
					{"number":50},
					{"number":100}
				];