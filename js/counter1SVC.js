(function(){
    
    angular.module('myApp.counter1')
            .service('counter1SVC',['$http','$q',
                                   
               function($http,$q){
                   this.getData = function(){
                    var data = $q.defer();   
                   $http.get("data/counter1.csv").then(function(response){
                       if(response.status == 200)
                            {
                                data.resolve(response);
                            }
                        else{
                            data.reject(response);
                        }
                       
                   });
                    return data.promise;
                   
               }
                             
               }
                                   
        ])
    
})();


 
            

