angular.module('myApp.simmAdminMS',[])
     //SIMM Admin Starts
            .factory('simmAdmin',function(){
                return{
                    tblHeadings: function()
                    {
                        return tableHeadings;
                    },
                    tbl1: function()
                    {
                        return tbl1Data;
                    },
                    tbl2: function()
                    {
                        return tbl2Data;
                    },
                    tbl3: function()
                    {
                        return tbl3Data;
                    },
                    tbl4 : function()
                    {
                        return tbl4Data;
                    },
                    statusTblHeading: function()
                    {
                        return statusTbl;
                    },
                    statusTblContent: function()
                    {
                        return statusTblData;
                    }
                }
        
        
            })
            //SIMM Admin Ends

 //SIMM Admin Table Headers
    var tableHeadings = [{"config":"Config","setting":"Setting"}];


  var tbl1Data = [
        {"config":"Server","setting":"LNPS101"},
        {"config":"Port","setting":12021},
        {"config":"Environment","setting":"Production"},
        {"config":"Type","setting":"Primary"}
    ];
    
    var tbl2Data = [
        {"config":"Server","setting":"ACPS101"},
        {"config":"Port","setting":12021},
        {"config":"Environment","setting":"Production"},
        {"config":"Type","setting":"Secondary"}
        
    ];
    
    var tbl3Data = [
        
        {"config":"Data","setting":"Prices"},
        {"config":"Server","setting":"XYZ101"},
        {"config":"Port","setting":5000},
        {"config":"Environment","setting":"Production"}
    ];
    
    var tbl4Data = [
        
        {"config":"Data","setting":"Sensitivities"},
        {"config":"Server","setting":"XYZ101"},
        {"config":"Port","setting":5000},
        {"config":"Environment","setting":"Production"}
    ];
    
    var statusTbl = [
        {"dataItem":"Data Item","status":"Status"}
    ];
    
    var statusTblData = [
         {"dataItem":"Trade Load","status":"COMPLETE"},
        {"dataItem":"LIBOR Fixings","status":"COMPLETE"},
        {"dataItem":"Bond Prices","status":"COMPLETE"},
        {"dataItem":"FX Prices","status":"UNAVAILABLE"},
        {"dataItem":"SIMM Calculation","status":"IN PROGRESS"}
    ];