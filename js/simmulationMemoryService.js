angular.module('myApp.simmulationMS',[])
 //Simmulation Starts
            
            .factory('simmulationData',function(){
                return{
                    query: function()
                    {
                        return simms;
                    },
                    lordPortfolios: function()
                    {
                        return portFolios;
                    },
                    lordCountries: function()
                    {
                        return lordCountries;
                    },
                    loadHeaders: function()
                    {
                        return riskHeaders;
                    },
                    tradeContent: function()
                    {
                        return tradeContents;
                    }
                }
        
        
            })
            //Simmulation Ends


 var simms = [
					{"SimulationDate":"Cell 7", "SimulationID":"Cell 8", "SimulationDescription":"Cell 9","Select":""},
					{"SimulationDate":"Cell 10", "SimulationID":"Cell 11", "SimulationDescription":"Cell 12","Select":""},
					{"SimulationDate":"1/2/2016", "SimulationID":"20160102-Production", "SimulationDescription":"EOD Run","Select":""},
					{"SimulationDate":"1/2/2016", "SimulationID":"20160102-IRS Shock", "SimulationDescription":"Teds Report","Select":""}
					
				
				];

//Simmulation Portfolios data
    var portFolios = [
        {"lordP":"Bank of America North East Gas Portfolio"}
    ];


  //Simmulation LordCountries data
    var lordCountries = [
        {"lordC":"Bank of America"}
    ];

var riskHeaders = [
        {"counter":"Counter Party","risk":"Risk Category","cur":"Currency","delta":"Delta Margin","Vega":"Vega Margin","curvarture":"Curvature Margin","total":"Total" }
    ];


var tradeContents = [
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Rates and FX", "currency":"EUR", "deltaMargin":500253.11, "vegaMargin":500167.97, "curvatureMargin":323253.64, "Total":323253.64},
         
        {"counterParty":"JP Morgan & Chase","trade":1234, "riskCategory":"Credit", "currency":"EUR", "deltaMargin":685862.81, "vegaMargin":-685459.48, "curvatureMargin":512505.69, "Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Equity","currency":"USD","deltaMargin":	984007.93,"vegaMargin":982784.74,"curvatureMargin":808002.27,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":1234,"riskCategory":"Commodity","currency":"USD","deltaMargin":11251118.55,"vegaMargin":1248366.73,"curvatureMargin":1073751.64,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Rates and FX","currency":"EUR","deltaMargin":500253.11,"vegaMargin":500167.97,"curvatureMargin":323253.64,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Credit","currency":"EUR","deltaMargin":685862.81,"vegaMargin":-685459.48,"curvatureMargin":512505.69,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Equity","currency":"USD","deltaMargin":984007.93,"vegaMargin":982784.74,"curvatureMargin":808002.27,"Total":323253.64},
        
        {"counterParty":"JP Morgan & Chase","trade":5555,"riskCategory":"Commodity","currency":"USD","deltaMargin":11251118.55,"vegaMargin":1248366.73,"curvatureMargin":1073751.64,"Total":323253.64},
        
        {"counterParty":"Total","trade":"","riskCategory":"","currency":"","deltaMargin":1251118.55,"vegaMargin":-1248366.73,"curvatureMargin":-1073751.64,"Total":323253.64}
    ];
    