'use strict';

var main = angular.module('myApp.controllers',[])
        //Dashboard Controller
		.controller('dashboardController',['$scope','prouctsList', '$rootScope',
			function($scope,prouctsList,$rootScope){
				
				$scope.title = "Product View";
                $scope.headers = prouctsList.tableHead();
				
				var listLoad = true;
				
				/*if(listLoad === true)
				{
					$scope.products = prouctsList.query();
					$scope.arrLength = $scope.products.length;
                    
				} */
                
				
                $scope.entries = prouctsList.selectBox();
                
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
			
                
               $scope.reports = [];
    
    var companyDetails = [
        {
        csvData: 
            `"CompanyParty", "SIMMRates", "SIMMCredit", "SIMMEquity", "SIMMCommodity", "TotalSIMM"
            "JP Morgan","1323674.72","512909.02","2774794.94","0","4611378.68"
            "Citigroup","0","0","0","512909.02","512909.02"
            "Total",,,,,,"5124287.70"`
        }
        
    ];
    
    
    angular.forEach(companyDetails, function(element, index){
            var csvData = element.csvData;
            var rows = csvData.replace(/"/g, '').split(/\n/g);
            var report ={
                //name: element.month,
                data:[]
                
            }
            angular.forEach(rows, function(element,index){
                if(index !== 0)
                {
                    var rowData = element.split(/,/g);
                    var tempReportData = {
                        CompanyParty: rowData[0].trim(),
                        SIMMRates: rowData[1].trim(),
                        SIMMCredit: rowData[2].trim(),
                        SIMMEquity: rowData[3].trim(),
                        SIMMCommodity: rowData[4].trim(),
                        TotalSIMM: rowData[5].trim(),
                    }
                    report.data.push(tempReportData);
                }
                
            });
            $scope.reports.push(report);
        }); 
                
			}])
			
            //Counterparty 2 Controller
			.controller('counterparty2Controller',['$scope','riskCategory','$rootScope',
                function($scope,riskCategory,$rootScope){
				
				$scope.title = "Risk Category";
                $scope.counter2Headers = riskCategory.tableHead();
                
                var riskLoad = true;
                if(riskLoad === true)
                {
                    $scope.products = riskCategory.query(); 
                    $scope.prodLength = $scope.products.length;
                    
                }
                $scope.entries = riskCategory.selectBox();
               if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                    
			}])
            
            //Counterparty1 Controller
            .controller('counterparty1Controller',['$scope','SIMMDetails','$rootScope',
             function($scope,SIMMDetails,$rootScope){
                $scope.title ="SIMM Details";
                $scope.counter1Header = SIMMDetails.tableHead();
                 
                var riskLoad = true;
                if(riskLoad === true)
                {
                    $scope.Products = SIMMDetails.query();
                    $scope.prodLength = $scope.Products.length;
                    
                }
                $scope.entries = SIMMDetails.selectBox();
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                    
               
            }])
            
            //Commodity-trade Controller 
           .controller('commodityController',['$scope','commodityTrade','$rootScope',
            function($scope,commodityTrade,$rootScope){
               $scope.title = "SIMM Views : Commodity Trade Details";
               
               var commodityLoad = true;
               if(commodityLoad === true)
                {
                    $scope.commodities = commodityTrade.query();
                     $scope.prodLength = $scope.commodities.length;
                }
               $scope.entries = commodityTrade.selectBox();
               if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
               
           }])
            
            // Credit Traders Controller
            .controller('creditTradeController',['$scope','creditTrades','$rootScope',
                function($scope,creditTrades,$rootScope){
                $scope.title = "SIMM Views : Credit Trade Details";
                
                var creditTrade = true;
                if(creditTrade === true)
                {
                    $scope.creditTrades = creditTrades.query();
                    $scope.prodLength = $scope.creditTrades.length;
                    
                }
                    $scope.entries = creditTrades.selectBox();
                    if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
            }])

            //Equidity Trade Controller

            .controller('equityTradeController',['$scope','equityTrades','$rootScope',
                function($scope,equityTrades,$rootScope){
                $scope.title = "SIMM Views : Equity Trade Details";
                
                var equityTrade = true;
                if(equityTrade === true)
                {
                    $scope.equityTrades = equityTrades.query();
                     $scope.prodLength = $scope.equityTrades.length;
                }
                $scope.entries = equityTrades.selectBox();
               if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                
            }])

            //Counterparty dupil controller
            .controller('counterpartyDupliController',['$scope','counterPartyDupil','$rootScope',
                function($scope,counterPartyDupil,$rootScope){
                $scope.title = "Portfolio";
                
                var counterTrade = true;
                if(counterTrade === true)
                {
                    $scope.counterDuplies= counterPartyDupil.query();
                    $scope.prodLength = $scope.counterDuplies.length;
                }
                $scope.entries = counterPartyDupil.selectBox();
               if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
            }])

            //Simmulation Controller
            .controller('simmulationController',['$scope','simmulationData', '$rootScope',function($scope,simmulationData,$rootScope){
                $scope.title="Customize Inputs – Market Data : Simmulation";
                
                var simmulation = true;
                
                if($rootScope.simmulation)
                {
                    $scope.txtField = $rootScope.simmulation;
                    $scope.radioCheck = $rootScope.simmulation;
                    
                }
                else
                {
                   $scope.txtField = "20160102-Production";
                   $scope.radioCheck = "20160102-Production";
                   $rootScope.simmulation = "20160102-Production";
                }
               
                
                if(simmulation === true)
                {
                    $scope.simms = simmulationData.query();
                    $scope.prodLength = $scope.simms.length;
                }
                $scope.fun = function(data)
                {
                   $scope.txtField = data;
				   $rootScope.simmulation = $scope.txtField;                   
                    
                }
                $scope.lordPortfolios = simmulationData.lordPortfolios();
                $scope.lordCountries = simmulationData.lordCountries();
                $scope.tradeHeaders = simmulationData.loadHeaders();
                $scope.tradeContents = simmulationData.tradeContent();
                
            }])

            //PersonalWorkSpace Controller
            .controller('personalController',['$scope','$rootScope','personControl',
                function($scope,$rootScope,personControl){
                $scope.title = "SIMM Actions: Personal Workspace";
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                $scope.startingPages = [
                    {"name":"SIMM Dashboard"}
                ];
                $scope.customeViewName = [
                    {"name":"JP Morgan, Energy Deriv Portfolio"}
                ];
                     $scope.branchAmounts = [
                        {"amount":1000}
                    ];
                
                var personCtrl = true;
                if(personCtrl === true)
                {
                    $scope.portfolios = personControl.query();
                    $scope.branchRules = personControl.branchRul();
                   
                }
                
            }])

            // Regular Controller
            .controller('regularController',['$scope','$rootScope','riskControl',
                function($scope,$rootScope,riskControl){
                
                $scope.title = "SIMM Views: SIMM by Risk Category";
                
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                    
                $scope.riskCategories = riskControl.query();
                $scope.arrLength = $scope.riskCategories.length;
                $scope.entries = riskControl.selectBox(); 
                $scope.riskHeaders = riskControl.riskHeader(); 
                    
                
                
            }])

            //Cutsome Controller

            .controller('customController',['$scope','$rootScope','customeControl',
                function($scope,$rootScope,customeControl){
                $scope.title = "Product View -> Custom SIMM Details";
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
                 $scope.customeHeaders = customeControl.custHeader();   
                 $scope.customeCategories =  customeControl.query();
                 $scope.arrLength = $scope.customeCategories.length;
                $scope.entries = customeControl.selectBox(); 
            }])

            //SIMM Actions Controller
            .controller('SIMM_ActionsController',['$scope','$rootScope','simmView',
                function($scope,$rootScope,simmView){
                    $scope.title = "SIMM Actions: CRIF Output";
                     if($rootScope.simmulation)
                        {
                           $scope.mode = $rootScope.simmulation;
                        }
                        else
                        {
                            $scope.mode = "20160102-Production";   
                        }
                    $scope.simmHeaders = simmView.custHeader();
                    $scope.simmContents = simmView.query();
                    $scope.arrLength = $scope.simmContents.length;
                    $scope.blazerTitle = "SIMM Breaches Overview COB April 10, 2016";
                    $scope.blazerHeaders = simmView.blazerHeader();
                    $scope.blazerContents = simmView.queryBlazer();
                    $scope.blazeerLength = $scope.blazerContents.length;

                }])

                //SIMM Report Controller
                .controller('SIMM_ReportsController',['$scope','$rootScope','simmReport',
                    function($scope,$rootScope,simmReport){
                    $scope.title = "SIMM Reports";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    $scope.simmAttrHeaders = simmReport.attrHeader();
                    $scope.simmAttrContents = simmReport.query();
                    $scope.arrLength = $scope.simmAttrContents.length;
                    $scope.simmCSAHeaders = simmReport.csaHeaders();
                    $scope.simmCSAContents = simmReport.csaContent();
                    
                }])

                //SIMM Admin Controller
                .controller("SIMM_AdminController",['$scope','$rootScope','simmAdmin',
                    function($scope,$rootScope,simmAdmin){
                    $scope.title = "SIMM Admin";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    
                    $scope.configInterface1 = "Blazer Connection: Primary";
                    $scope.configInterface2 = "Blazer Connection: Secondary";
                    $scope.configInterface3 = "Reference Data Connectivity: 1";
                    $scope.configInterface4 = "Reference Data Connectivity: 2";
                    $scope.tblHeadings = simmAdmin.tblHeadings();
                    $scope.configTblData1 = simmAdmin.tbl1();
                        $scope.tb1Length = $scope.configTblData1.length;
                    $scope.configTblData2 = simmAdmin.tbl2();
                        $scope.tb2Length = $scope.configTblData2.length;
                    $scope.configTblData3 = simmAdmin.tbl3();
                        $scope.tb3Length = $scope.configTblData3.length;
                    $scope.configTblData4 = simmAdmin.tbl4();
                         $scope.tb4Length = $scope.configTblData4.length;
                    $scope.statusInterfaceTitle = "SIMM Data Availability for COB April 04, 2016";
                    $scope.statusTblHeadings = simmAdmin.statusTblHeading();
                    $scope.statusTblContents = simmAdmin.statusTblContent();
                    $scope.statusLength = $scope.statusTblContents.length;
                    
                }])
                 
                // Reference Data Views controler
                .controller('reference_data_viewController',['$scope','$rootScope','refData',
                    function($scope,$rootScope,refData){
                    $scope.title = "Reference Data Views : Calenders";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    $scope.curveTitles = refData.curvTitle();
                    $scope.curveTableData = refData.curvData();
                        $scope.statusLength = $scope.curveTableData.length;
                    $scope.calenderCountry = refData.country();
                    $scope.holidaysHeading = refData.holidayHeading();
                    $scope.holidayLists = refData.holidayList();
                    $scope.priceHeaders = refData.priceHeader();
                    $scope.priceContents = refData.priceContent();
                        $scope.priceLength = $scope.priceContents.length;
                    $scope.generalTableHeaders = refData.generalTableHeader();
                    $scope.generalTableContents = refData.generalTableContent();
                        $scope.generalLength = $scope.generalTableContents.length;
                    
                        
                    //GIRR
                    $scope.girrTbl1 = refData.girrTbl1();
                        $scope.girrlLength = $scope.girrTbl1.length;
                    
                    $scope.girrTbl2 = refData.girrTbl2();
                         $scope.girr2lLength = $scope.girrTbl2.length;
                        
                    $scope.girrTbl3 = refData.girrTbl3();
                         $scope.girr3lLength = $scope.girrTbl3.length;
                        
                    $scope.girrTbl4 = refData.girrTbl4();
                         $scope.girr4lLength = $scope.girrTbl4.length;
                        
                    
                    //Credit Qualifying
                        
                    $scope.creditTbl1 = refData.credTbl1();
                         $scope.creditTbl1Length = $scope.creditTbl1.length;
                    
                    $scope.creditTbl2 = refData.credTbl2();
                        $scope.creditTbl2Length = $scope.creditTbl2.length;
                        
                    $scope.creditTbl3 = refData.credTbl3();
                        $scope.creditTbl3Length = $scope.creditTbl3.length;
                        
                    $scope.creditTbl4 = refData.credTbl4();
                         $scope.creditTbl4Length = $scope.creditTbl4.length;
                        
                    // Credit Non Quailfying
                        
                    $scope.nonCreditTbl1 = refData.nonCreditTbl1();
                        $scope.nonCreditTbl1Length = $scope.nonCreditTbl1.length;
                    
                    $scope.nonCreditTbl2 = refData.nonCreditTbl2();
                        $scope.nonCreditTbl2Length = $scope.nonCreditTbl2.length;
                        
                    $scope.nonCreditTbl3 = refData.nonCreditTbl3();
                         $scope.nonCreditTbl3Length = $scope.nonCreditTbl3.length;
                        
                    $scope.nonCreditTbl4 = refData.nonCreditTbl4();
                        $scope.nonCreditTbl4Length = $scope.nonCreditTbl4.length;
                        
                    //Commodity
                    $scope.commodityTbl1 = refData.comodity1();
                         $scope.commodityTbl1Length = $scope.commodityTbl1.length;
                    $scope.commodityTbl2 = refData.comodity2();
                        $scope.commodityTbl2Length = $scope.commodityTbl2.length;
                    $scope.commodityTbl3 = refData.comodity3();   
                        $scope.commodityTbl3Length = $scope.commodityTbl3.length;
                    
                    $scope.commodityTbl4 = refData.comodity4();
                        $scope.commodityTbl4Length = $scope.commodityTbl4.length;
                    $scope.commodityTbl5 = refData.comodity5();
                         $scope.commodityTbl5Length = $scope.commodityTbl5.length;
                        
                    //FX
                    $scope.fxs = refData.fxs();
                        $scope.fxsLength = $scope.fxs.length;
                        
                    
                }]);

               



			
			
			
