    
(function () {
    'use strict';
    
    var dashboardController = function($scope,dashboardSVC,$rootScope,prouctsList){
        $scope.title = "Product View";
        $scope.headers = prouctsList.tableHead();
        
                if($rootScope.simmulation)
                {
                   $scope.mode = $rootScope.simmulation;
                }
                else
                {
                    $scope.mode = "20160102-Production";   
                }
        
      
          dashboardSVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                   
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                CompanyParty:data[0].trim(),
                                 SIMMRates: data[1].trim(),
                        SIMMCredit: data[2].trim(),
                        SIMMEquity: data[3].trim(),
                        SIMMCommodity: data[4].trim(),
                        TotalSIMM: data[5].trim(),
                        a:data[6].trim(),       
                            } 
                                
                            
                            
                   
                        }
                        lines.push(tarr);
                  
                    
                }
                $scope.data = lines;
            
            
            
        });
       
    };
               

    
    
    
    
    angular.module('myApp.dashboard',[])
    .controller('dashboardController', ['$scope','dashboardSVC','$rootScope','prouctsList',dashboardController])


    

    })();