

(function(){
    
    var counterpartyDupliController = function($scope,counterDupilSVC,$rootScope,counterPartyDupil){
        
         $scope.title = "Portfolio";
         $scope.entries = counterPartyDupil.selectBox();
         if($rootScope.simmulation)
         {
            $scope.mode = $rootScope.simmulation;
         }
         else
         {
            $scope.mode = "20160102-Production";   
         }
        
        counterDupilSVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                RiskCategory:data[0],
                                CounterParty: data[1],
                                CSAID: data[2],
                                TradingDesk: data[3],
                                Portfolio:data[4],
                                Currency: data[5],
                                DeltaMargin: data[6],
                                VegaMargin:data[7],   
                                CurvatureMargin:data[8],   
                                Total:data[9],   
                            } 
                           
                           
                        }
                        lines.push(tarr);
                 
                }
                $scope.data = lines;
             ;
            
            
        });
        
        
    }
    
    
     angular.module('myApp.counterDupil',[])
    .controller('counterpartyDupliController', ['$scope','counterDupilSVC','$rootScope','counterPartyDupil',counterpartyDupliController])

})();