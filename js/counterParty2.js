
(function(){
    
    
    var counterparty2Controller = function($scope,counter2SVC,$rootScope,riskCategory){
        
        
        $scope.title = "Risk Category";
        $scope.counter2Headers = riskCategory.tableHead();
        
         $scope.entries = riskCategory.selectBox();
         if($rootScope.simmulation)
         {
                 $scope.mode = $rootScope.simmulation;
         }
         else
         {
               $scope.mode = "20160102-Production";   
          }
        
        
        
        
         counter2SVC.getData().then(function(allText){
            
            allText = allText.data; 
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                var lines = [];

                for ( var i = 0; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                  
                        var tarr = [];
                       
                        for ( var j = 0; j < headers.length; j++) {
                         
                           tarr = {
                                RiskCategory:data[0],
                                 CounterParty: data[1],
                                CSAID: data[2],
                                TradingDesk: data[3],
                                Currency: data[4],
                                DeltaMargin: data[5],
                                VegaMargin:data[6],   
                                CurvatureMargin:data[7],   
                                Total:data[8],   
                            } 
                           
                          
                            
                            
                   // report.data.push(tarr);
                        }
                        lines.push(tarr);
                   // }
                    
                }
                $scope.data = lines;
             // scope.reports.push(lines);
             // $scope.reports.push(report);
            // console.log($scope.data);
            
            
        });
        
    };
    
     angular.module('myApp.counter2',[])
    .controller('counterparty2Controller', ['$scope','counter2SVC','$rootScope','riskCategory',counterparty2Controller])
})();