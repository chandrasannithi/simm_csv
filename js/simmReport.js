angular.module('myApp.simmReport',[])
        .controller('SIMM_ReportsController',['$scope','$rootScope','simmReport',
                    function($scope,$rootScope,simmReport){
                    $scope.title = "SIMM Reports";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    $scope.simmAttrHeaders = simmReport.attrHeader();
                    $scope.simmAttrContents = simmReport.query();
                    $scope.arrLength = $scope.simmAttrContents.length;
                    $scope.simmCSAHeaders = simmReport.csaHeaders();
                    $scope.simmCSAContents = simmReport.csaContent();
                    
                }])