angular.module('myApp.simmAdmin',[])
         .controller("SIMM_AdminController",['$scope','$rootScope','simmAdmin',
                    function($scope,$rootScope,simmAdmin){
                    $scope.title = "SIMM Admin";
                     if($rootScope.simmulation)
                    {
                       $scope.mode = $rootScope.simmulation;
                    }
                    else
                    {
                        $scope.mode = "20160102-Production";   
                    }
                    
                    $scope.configInterface1 = "Blazer Connection: Primary";
                    $scope.configInterface2 = "Blazer Connection: Secondary";
                    $scope.configInterface3 = "Reference Data Connectivity: 1";
                    $scope.configInterface4 = "Reference Data Connectivity: 2";
                    $scope.tblHeadings = simmAdmin.tblHeadings();
                    $scope.configTblData1 = simmAdmin.tbl1();
                        $scope.tb1Length = $scope.configTblData1.length;
                    $scope.configTblData2 = simmAdmin.tbl2();
                        $scope.tb2Length = $scope.configTblData2.length;
                    $scope.configTblData3 = simmAdmin.tbl3();
                        $scope.tb3Length = $scope.configTblData3.length;
                    $scope.configTblData4 = simmAdmin.tbl4();
                         $scope.tb4Length = $scope.configTblData4.length;
                    $scope.statusInterfaceTitle = "SIMM Data Availability for COB April 04, 2016";
                    $scope.statusTblHeadings = simmAdmin.statusTblHeading();
                    $scope.statusTblContents = simmAdmin.statusTblContent();
                    $scope.statusLength = $scope.statusTblContents.length;
                    
                }])